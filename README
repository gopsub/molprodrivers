
These are a set of utilities that use MolPro as the backend for force calls,
and perform a variety of PES calculations. The utilities have been tested
with Python 2.7.

============================================================================
Prerequisites for running all drivers:

1. MolPro:      a serial or parallel version of MolPro is required.

2. Environment: The location of the molpro executable (along with required
                flags) must be set in the environment variable MOLPRO_EXEC
		   
2. Python:      Version 2.7 is the only one that has been tested. Other
                versions may work, but try them at your own risk

============================================================================
General information for all drivers:

0. All drivers are run using
         python <driver> <casename>

   e.g.  python stretch.py ethane
   
1. <casename> is used as the root for all input and output files

2. Input parameters are read from <casename>.input

3. Configurations are in standard .XYZ file format. The format is:
   line 1: number of atoms
   line 2: comment
   line 3 onwards: Atom position, one line per atom, formatted as:
                   <Chemical Symbol> X Y Z

4. The input file for all drivers contains a block that contains setup of
   the Molpro calculations. These are molpro-specific commands that will be
   passed to MolPro when invoked. The start of this block is specified by
   the line 'MOLPRO-PARAMETERS' in the input file

5. For all input files, all text following a '#' sign is ignored

6. Directories called <casename>.image.N are created, and used as the 
   MolPro working directories. If these directories exist, then the
   drivers will not start running.

============================================================================

STRETCH.PY: A driver to find the force-extension curve of a molecule

Algorithm:
1. The starting configuration is read from <casename>.start. The last atom
   in this configuration is called the 'Anchor', and the second-to-last is
   the 'Puller'
2. The Anchor is moved to the origin
3. Entire molecule is rotated, so that the Puller is on the X-axis
4. Optionally, an energy minimization is performed
5. The Puller is moved by a small amount, dx, along the X-axis
6. The structure is optimized, and the force on the Puller is recorded
7. Repeat from step 5

Input file format:
Line  1   : distance moved during each step
Lines 2-4 : Minimization parameters
Line  5   : Max number of steps
Line  6   : Whether or not the start configuration should be minimized.
            Possible choices are "NONE" and "START"
============================================================================
