from FileUtility import *
import os
import commands
import sys
import time
import AbInit, SimPar
import ExtraForceUtility
import subprocess

#====================================================================================
def GetAbInitCount(procs):
#====================================================================================
    count = 0
    for i in range(len(procs)):
        if procs[i] != None:
            if procs[i].poll() == None:
                count = count + 1
    return count

#====================================================================================
def ExecAbInit(WorkDir):
#   Routine that enters the directory, runs Ab Initio calculation, and comes back out.
#   This routine will wait until all instances are finished before returning
#   control to the caller
#   WARNING: This is a bit klunky, and might need revision when going to run simulations on the clusters
#====================================================================================
    
    try:
        AbInitExec = os.environ['MOLPRO_EXEC']
    except:
        print '------> Environment Variable MOLPRO_EXEC is not set. <------'
        raise
    
    NSim = len(WorkDir)
    MaxJobs   = copy.deepcopy(SimPar.MaxJobs)
    SleepTime = copy.deepcopy(SimPar.SleepTime)
    procs     = [None]*NSim
    
    print 'Submitting ',NSim, ' jobs ... '
    for isim in range(NSim):
        while True:
            if GetAbInitCount(procs) < MaxJobs:
                # Delete old files
                execstring = 'cd '+WorkDir[isim]+' ;if [ -e molpro.log ]; then rm molpro.log;fi;if [ -e molpro.out ]; then rm molpro.out;fi; if [ -e molpro.xml ]; then rm molpro.xml;fi '
                os.system(execstring)

                #Fire up Molpro
                runningpath=os.getcwd()+'/'+WorkDir[isim]
                procs[isim] = subprocess.Popen(AbInitExec+' -W ./ -d ./ molpro.inp',cwd=runningpath,shell=True)
                print isim,',',
                sys.stdout.flush()
                break
            else:
                time.sleep(SleepTime)

    
    print '\nWaiting for ',GetAbInitCount(procs),' MolPro jobs to finish'
    while True:

        print GetAbInitCount(procs),
        if GetAbInitCount(procs) == 0:
            print 'Done!'
            break
        sys.stdout.flush()
        time.sleep(SleepTime)
    
    print 'WARNING: NOT CHECKING IF AbInit COMPLETED SUCCESSFULLY'

#====================================================================================
def CreateMassArray(ChemSym):
#====================================================================================
    Natom = len(ChemSym)
    Mass = numpy.zeros((Natom,3),dtype=float)
    for i in range(Natom):
        if ChemSym[i] == 'H':
            m = 1.00794
        elif ChemSym[i] == 'He':
            m = 4.002602
        elif ChemSym[i] == 'Li':
            m = 6.941
        elif ChemSym[i] == 'Be':
            m = 9.012182
        elif ChemSym[i] == 'B':
            m = 10.811
        elif ChemSym[i] == 'C':
            m = 12.0107
        elif ChemSym[i] == 'N':
            m = 14.00674
        elif ChemSym[i] == 'O':
            m = 15.9994
        elif ChemSym[i] == 'F':
            m = 18.9984032
        elif ChemSym[i] == 'Ne':
            m = 20.1797
        elif ChemSym[i] == 'Na':
            m = 22.989770
        elif ChemSym[i] == 'Mg':
            m = 24.3050
        elif ChemSym[i] == 'Al':
            m = 26.981538
        elif ChemSym[i] == 'Si':
            m = 28.0855
        elif ChemSym[i] == 'P':
            m = 30.973761
        elif ChemSym[i] == 'S':
            m = 32.066
        elif ChemSym[i] == 'Cl':
            m = 35.4527
        elif ChemSym[i] == 'Ar':
            m = 39.948
        else:
            print 'Chem Symbol ',ChemSym[i], ' mass not defined'
            raise

        for j in range(3):
            Mass[i][j] = m

    return(Mass)

#====================================================================================
def multiplyHC_global(HG,C):
# 
# A function that takes in 
#     HG = Global Hessian   Nimage * 3*Natom x Nimage * 3*Natom
#      C = A chain of configurations, [Natom x 3] * Nimage
#
# Computes C-bar = C raveled into a Nimage*Natom*3 x 1 array,
# Computes alpha-bar = H * C-bar (matrix multipication),
# Returns alpha = alpha-bar packed into a list, with Nimage elements, and each element is a Natom x 3 array
#====================================================================================
    nimage = len(C)
    natom  = C[0].shape[0]
    ndof   = HG.shape[0]
    ndofpi = natom*3      # ndof per image

    if HG.shape[0] != HG.shape[1]:
        print 'Trying to multiply Global HG and C, did not get a square HG'
        raise
    if nimage*ndofpi != ndof:
        print 'Trying to multiply Global HG and C, Ndof is not compatible with Natom and Nimage'
        raise

    # Ravel all configs, and put them into a single vector, Cbar
    Cbar = numpy.reshape(numpy.array([],dtype='float'),(0,1))
    for i in range(nimage):
        Cbar = numpy.append(Cbar,numpy.ravel(C[i],'C'))

    # Matrix multiply HG and Cbar
    alpha_bar = numpy.dot(HG,numpy.reshape(Cbar,(ndof,1)))

    alpha = [None]*nimage
    for i in range(nimage):
        alpha[i] = numpy.reshape(alpha_bar[i*ndofpi:(i+1)*ndofpi],(natom,3))

    return(alpha)

#====================================================================================
def multiplyHC(H, C):
#
# A function that takes in 
#     H = a Hessian,               3*Natom x 3*Natom, and
#     C = an atomic configuration,   Natom x 3 
# 
# Computes C-bar = C raveled into a Natom*3 x 1 array,
# Computes alpha-bar = H * C-bar (matrix multipication),
# Returns alpha = alpha-bar packed into a Natom x 3 array
#====================================================================================
    ndof  = H.shape[0]
    natom = C.shape[0]
    if ndof != natom*3:
        print 'multiplying H and C, shapes do not match'
        print 'Shape of H and C passed in are',H.shape,C.shape
        raise

    ret = numpy.reshape(numpy.dot(H,numpy.reshape(numpy.ravel(C,'C'),(ndof,1))),(natom,3))

    return(ret)

#====================================================================================
def updateHG(H, s, y):
# Global Hessian update, from Numerical Optimization, Nocedal & Wright, equation 8.16 & 8.17
# Unravels s and y before updating H
#====================================================================================
    nimage = len(s)
    ndof   = H.shape[0]
    natom  = s[0].shape[0]
    ndofpi = natom*3

    if nimage*ndofpi != ndof:
        print 'Updating Global H, shapes passed in do not match'
        raise

    I = numpy.identity(ndof,dtype='float')
    s2 = numpy.array([],dtype='float')
    y2 = numpy.array([],dtype='float')
    for i in range(nimage):
        s2 = numpy.append(s2,numpy.ravel(s[i],'C'))
        y2 = numpy.append(y2,numpy.ravel(y[i],'C'))
    s2 = numpy.reshape(s2,(ndof,1))
    y2 = numpy.reshape(y2,(ndof,1))

    rho  = 1/numpy.dot(numpy.transpose(y2),s2)[0][0]
    M1   = I - rho*numpy.dot(s2,numpy.transpose(y2))
    M3   = I - rho*numpy.dot(y2,numpy.transpose(s2))
    M4   =     rho*numpy.dot(s2,numpy.transpose(s2))
    Hnew = numpy.dot(numpy.dot(M1,H),M3) + M4
    return Hnew

#====================================================================================
def updateH(H, s, y):
# Hessian update, from Numerical Optimization, Nocedal & Wright, equation 8.16 & 8.17
#====================================================================================
    ndof  = H.shape[0]
    natom = s.shape[0]
    if ndof != natom*3:
        print 'Updating H: Shapes of arrays passed in do not match'
        raise

    I   = numpy.identity(ndof,dtype='float')
    s2  = numpy.reshape(numpy.ravel(s,'C'),(ndof,1))
    y2  = numpy.reshape(numpy.ravel(y,'C'),(ndof,1))
    rho = 1/numpy.dot(numpy.transpose(y2),s2)[0][0]

    M1   = I - rho*numpy.dot(s2,numpy.transpose(y2))
    M3   = I - rho*numpy.dot(y2,numpy.transpose(s2))
    M4   =     rho*numpy.dot(s2,numpy.transpose(s2))
    Hnew = numpy.dot(numpy.dot(M1,H),M3) + M4

    return Hnew

  

#====================================================================================
def ManualMinimize(config, WorkDir, Minimizer, CaseName, drcrit, dEcrit, Fcrit):
#====================================================================================


    Niter     =  0
    Nconfig   =  len(config)
    gfac      =  0.5
    dE        =  [None]*Nconfig
    Fmax      =  [None]*Nconfig
    drmax     =  [None]*Nconfig
    dr        =  [None]*Nconfig
    Converged = [False]*Nconfig

    # Initialize QuickMin
    if Minimizer == 'QM':
        gfac = 1.0
        dt = readQM(CaseName+'.input')

        Velocity = [None]*Nconfig
        for iconf in range(Nconfig):
            Velocity[iconf] = 0*config[0].Coord

    #Initialize Fire
    if Minimizer == 'FIRE':
        alpha_start, dt_start, dtmax, fdec_dt, finc_dt, falpha, Nadjustmin = readFIRE(CaseName+'.input')

        gfac = 1.0
        itercut     = 0
        Velocity    = [None]       *Nconfig
        Mass        = [None]       *Nconfig
        dt          = [dt_start]   *Nconfig
        alpha       = [alpha_start]*Nconfig
        for iconf in range(Nconfig):
            Velocity[iconf] = 0*config[0].Coord
            Mass[iconf]     = CreateMassArray(config[iconf].ChemSym)

    #Initialize BFGS
    if Minimizer == 'BFGS' or Minimizer == 'GBFGS':
        print 'BFGS Warning: Making the Hessian assuming that all DOFs are free'
        IHess = [numpy.identity(numpy.prod(config[0].Coord.shape),dtype='float')]*Nconfig


    print 'Manual Minimizer starting.'
    print 'Minimizer type = ',Minimizer
    print 'Hardwired parameters:'
    print 'Also taking the lazy way -- Will minimize every image, until all images are minimized'
    print 'gfac = ',gfac
    print 'dEcrit, Fcrit = ',dEcrit, Fcrit
    print 'drcrit = ',drcrit


    OriginalConfig = copy.deepcopy(config)


    while True:

        ###### COPY OLD SHIT ######
        LastConfig = copy.deepcopy(config)
        Lastdrstep = copy.deepcopy(dr)


        ###### GET ENERGIES, FORCES ######
        # Setup molpro input files. Do a HF prelude for first iteration only
        if (Niter == 0) and (AbInit.MolPro_Method != 'hf'):
            NewMethod = 'hf\n'+AbInit.MolPro_Method
        else:
            NewMethod = AbInit.MolPro_Method
        NewMethod = NewMethod+'\nforce;varsav;table,gradx,grady,gradz;format,\'d40.30,d40.30,d40.30\''
        CreateInput(config,WorkDir,NewMethod)

        #Run Molpro, Read Energies and Gradients
        ExecAbInit(WorkDir)
        ReadOutputEnergy(config,WorkDir)
        ReadOutputGradient(config,WorkDir)
        print 'E = ',[config[iii].E for iii in range(Nconfig)]

        #Add forces to all configs, if needed
        for iconfig in range(Nconfig):
            if SimPar.ExtraForceType == 1:
                config[iconfig].Gradient = config[iconfig].Gradient - SimPar.ExtraForce
                                                              # This negative sign is not a bug.
                                                              # Force is the negative of the gradient
            elif SimPar.ExtraForceType >= 2:
                config[iconfig].Gradient = config[iconfig].Gradient - ExtraForceUtility.FHydro(SimPar.ExtraForcePressure,config[iconfig].Coord)


        ###### COMPUTE STUFF FOR TERMINATION CRITERIA, AND CHECK FOR TERMINATION ######
        Terminate = False
        if Niter != 0:
            for iconfig in range(Nconfig):
                dE[iconfig] = config[iconfig].E - LastConfig[iconfig].E
                drmax[iconfig] = numpy.amax(abs(dr[iconfig]))
                Fmax[iconfig] = numpy.amax(abs(config[iconfig].Gradient))
                if ( abs(dE[iconfig]) < dEcrit ) and ( abs(drmax[iconfig]) < drcrit ) and ( abs(Fmax[iconfig]) < Fcrit ):
                    Converged[iconfig] = True

            print 'dE         = ',dE
            print 'drmax      = ',drmax
            print 'Fmax       = ',Fmax
            print 'Converged? = ',Converged
            if (max(map(abs,dE)) < dEcrit) and (max(map(abs,drmax)) < drcrit) and (max(map(abs,Fmax)) < Fcrit ):
                Terminate = True

        if Terminate:
            break


        ###### MOVE ATOMS, RESPECTING THE DOFMASK ARRAY ######
        # Compute dr, limit it, and add to config.
        for iconfig in range(Nconfig):
            if Converged[iconfig]:
                print 'KLUDGE -- Not going to move ',iconfig
                continue
            #-------------------------------------------------------------------------------------
            if Minimizer == 'SD':
                dr[iconfig] = -gfac*config[iconfig].Gradient

            #-------------------------------------------------------------------------------------
            elif Minimizer == 'CG':
                if Niter == 0:
                  dr[iconfig] = -gfac*config[iconfig].Gradient
                else:
                  gamma =         numpy.sum(numpy.multiply(config[iconfig].Gradient,config[iconfig].Gradient))
                  gamma = gamma / numpy.sum(numpy.multiply(LastConfig[iconfig].Gradient,LastConfig[iconfig].Gradient))
                  print 'gamma = ',gamma
                  dr[iconfig] = (-gfac*config[iconfig].Gradient) + gamma*( -gfac*LastConfig[iconfig].Gradient )

            #-------------------------------------------------------------------------------------
            elif Minimizer == 'QM':
                Force    = -copy.deepcopy(config[iconfig].Gradient)
                Fhat     = Force/(numpy.sqrt(numpy.sum(numpy.multiply(Force,Force))))
                VdotFhat = numpy.sum(numpy.multiply(Velocity[iconfig],Force))
                if VdotFhat < 0:
                    print '*** QuickMin-Zeroing velocity ***'
                    Velocity[iconfig] = 0*Velocity[iconfig]
                else:
                    Velocity[iconfig] = VdotFhat * Fhat

                #Euler step for positions and veocities
                dr[iconfig]       =                     dt*Velocity[iconfig]
                Velocity[iconfig] = Velocity[iconfig] + dt*Force

            #-------------------------------------------------------------------------------------
            elif Minimizer == 'FIRE':
                Force = -copy.deepcopy(config[iconfig].Gradient)
                ModF  =  numpy.sqrt(numpy.sum(numpy.multiply(Force,Force)))
                ModV  =  numpy.sqrt(numpy.sum(numpy.multiply(Velocity[iconfig],Velocity[iconfig])))
                Fhat  =  Force/ModF
                P     =  numpy.sum(numpy.multiply(Force,Velocity[iconfig]))
                Velocity[iconfig] = (1-alpha[iconfig])*Velocity[iconfig] + alpha[iconfig] * ModV * Fhat
                if P <= 0 and Niter > 0: # Steady on.
                    print 'iconf, Niter = ',iconfig,Niter
                    print 'Steady:'
                    Velocity[iconfig] = 0*Velocity[iconfig]
                    dt[iconfig]       = dt[iconfig]*fdec_dt
                    itercut           = Niter
                    alpha[iconfig]    = alpha_start
                elif P>0 and (Niter-itercut)>Nadjustmin: #Speed up
                    print 'iconf, Niter = ',iconfig,Niter
                    print 'Speed up, dt = ',dt[iconfig]
                    dt[iconfig]    = min(dt[iconfig]*finc_dt,dtmax)
                    alpha[iconfig] = alpha[iconfig] * falpha

                #Euler Integrate
                dr[iconfig] = dt[iconfig]*Velocity[iconfig]
                Velocity[iconfig] = Velocity[iconfig] + dt[iconfig]*(Force/Mass[iconfig])

            #-------------------------------------------------------------------------------------
            elif Minimizer == 'BFGS' or Minimizer == 'GBFGS':

                if Niter > 0:
                    s = Lastdrstep[iconfig]
                    y = config[iconfig].Gradient - LastConfig[iconfig].Gradient 
                    IHess[iconfig] = updateH(IHess[iconfig],s,y)

                dr[iconfig] = - multiplyHC(IHess[iconfig],config[iconfig].Gradient)

            mask = abs(dr[iconfig]) > drcrit
            dr[iconfig][mask] = drcrit*numpy.sign(dr[iconfig][mask])
            config[iconfig].Coord[SimPar.DOFMask] = config[iconfig].Coord[SimPar.DOFMask] + dr[iconfig][SimPar.DOFMask]
            appendXYZ(config[iconfig],'minimizing.'+str(iconfig))


        Niter += 1
        ########## End of loop 

    print 'Manual minimizer finished in ',Niter,'iterations'
    print '*****************************************'
    print ' *** WARNING *** WARNING *** WARNING *** '
    print '   MANUAL MINIMIZER WILL NOT MODIFY ENERGY'
    print '   The calling routine needs to account'
    print '   for energy changes due to applied'
    print '   forces'
    print '*****************************************'

#====================================================================================
def NativeMinimize(config, WorkDir):
#
#  Routine that uses MolPro's native minimizer (i.e. optg) to minimize config
#  This routine will do an HF call first
#====================================================================================
      
    print 'Native Minimizer starting'
    
    if AbInit.Software == 'MOLPRO':
        # Create ActiveString based on DOFMask
        ActiveString   = ['; active, '  ]
        InactiveString = ['; inactive, ']
        NactiveDOF   = 0
        NinactiveDOF = 0
        for iatom in range(SimPar.Natom):
            for j in range(3):
                if iatom == 0 and j== 0:
                    delim = '  '
                else:
                    delim = ', '
                if SimPar.DOFMask[iatom][j]:
                    ActiveString.append  (delim+'_'+str(iatom)+'_'+str(j))
                    NactiveDOF   += 1
                else:
                    InactiveString.append(delim+'_'+str(iatom)+'_'+str(j))
                    NinactiveDOF += 1
        ActiveString.append('\n')
        InactiveString.append('\n')

        # Replace ActiveString with InactiveString if needed.
        # This is a MolPro quirk.
        if NactiveDOF > 100 and NinactiveDOF > 100:
            print '------> Too many active and inactive DOFs. <------'
            raise
        elif NactiveDOF > 100 and NinactiveDOF < 100:
            ActiveString = InactiveString

        # Create the NewMethod to override the default method
        NewMethod = []
        NewMethod.append('hf\n')
        if AbInit.MolPro_Method != 'hf':
            NewMethod = NewMethod + [AbInit.MolPro_Method+'\n']
        NewMethod = NewMethod + ['optg'] + ActiveString


        CreateInput     (config, WorkDir, NewMethod)
        ExecAbInit      (WorkDir)
        ReadOutputConfig(config,WorkDir)
        ReadOutputEnergy(config,WorkDir)
    elif AbInit.Software == 'GAMESS':
        print 'Hello Polly - GAMESS Native Minimizer under construction'
        sys.exit()
    else:
        print '---> Invalid software choice in Native Minimizer <---'
        raise
    

