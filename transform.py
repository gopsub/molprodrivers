###
# Set of utilities for reading, writing, and manipulating atomic configurations
###


import numpy
import sys
import copy

#========================================================================================================  
def centroid(v):
#========================================================================================================  
    return numpy.mean(v,axis=0)

#========================================================================================================  
def MoveCentroidToOrigin(v):
#  Translate the entire configuration, so that the centroid is at the origin
#========================================================================================================  
    return v - numpy.tile(centroid(v),(v.shape[0],1))
      
#========================================================================================================  
def RotateAxisAngle(conf, unit_axis, angle):
#
#  Rotate the configuration about the origin, about <unit_axis>, by <angle> (radians)
#========================================================================================================  
    Natom = conf.shape[0]
    newconf = copy.deepcopy(conf)

    u = unit_axis[0]
    v = unit_axis[1]
    w = unit_axis[2]
    ct = numpy.cos(angle)
    st = numpy.sin(angle)

    for iatom in range(Natom):
        x = copy.deepcopy(conf[iatom][0])
        y = copy.deepcopy(conf[iatom][1])
        z = copy.deepcopy(conf[iatom][2])
        newconf[iatom][0] = u*(u*x+v*y+w*z)*(1-ct) + x*ct + (-w*y+v*z)*st
        newconf[iatom][1] = v*(u*x+v*y+w*z)*(1-ct) + y*ct + ( w*x-u*z)*st
        newconf[iatom][2] = w*(u*x+v*y+w*z)*(1-ct) + z*ct + (-v*x+u*y)*st

    return newconf
    

#========================================================================================================  
def rotatex(v):
#========================================================================================================  
  #Translate v to the origin
    v2 = MoveCentroidToOrigin(v)
    v3 = RotateAxisAngle(v2,numpy.array([1,0,0],dtype=float),0.01)
    return v3-v2

#========================================================================================================  
def rotatey(v):
#========================================================================================================  
  #Translate v to the origin
    v2 = MoveCentroidToOrigin(v)
    v3 = RotateAxisAngle(v2,numpy.array([0,1,0],dtype=float),0.01)
    return v3-v2

#========================================================================================================  
def rotatez(v):
#========================================================================================================  
  #Translate v to the origin
    v2 = MoveCentroidToOrigin(v)
    v3 = RotateAxisAngle(v2,numpy.array([0,0,1],dtype=float),0.01)
    return v3-v2

