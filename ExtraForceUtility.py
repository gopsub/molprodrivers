import numpy
import SimPar
import sys
import copy

#==========================================================================================
def Initialize(CaseName):
#==========================================================================================
    print 'Initializing extra forces array .. ',
    SimPar.ExtraForce = numpy.zeros((SimPar.Natom,3))


    if SimPar.ExtraForceType == 1:
        print 'Extra Force Type = Constant'
        try:
            ifile = open(CaseName+'.extraforce','r')
        except:
            print '------> Could not find file <casename>.extraforce <------'
            raise
        for iatom in range(SimPar.Natom):
            l = ifile.readline().split()
            for j in range(3):
                SimPar.ExtraForce[iatom][j] = float(l[j+1])
        ifile.close()

        print 'Constant Forces. WARNING Only one array for all images'

    elif SimPar.ExtraForceType >= 2:
        if SimPar.ExtraForceType == 2:
            print 'Extra Force type = Hydrostatic'
        else:
            print 'Extra Force type = Single coordinate.'

        try:
            ifile = open(CaseName+'.hydrostatic','r')
        except:
            print '------> Could not find file <casename>.hydrostatic <------'
            raise
        SimPar.ExtraForcePressure = float(ifile.readline().split()[0])
        ifile.close()
        print 'Hydrostatic, P = ',SimPar.ExtraForcePressure

    else:
        print 'None'

    print '--------------------------------------------------'
      
#==========================================================================================
def FHydro(P,Coord):
# Reminder : negative pressure = compressive, so, should be directed TOWARDS centroid'
#==========================================================================================
  
  centroid = numpy.mean(Coord,axis=0)
  F        = 0*Coord
  for iatom in range(SimPar.Natom):
      vec = Coord[iatom] - centroid       # Vector from centroid to atom

      if SimPar.ExtraForceType == 2:
          F[iatom] = P*vec         # If P>0, F is in a direction from centroid to atom (tensile)
                                     # If P<0, F is in a direction from atom to centroid (compressive)
      elif SimPar.ExtraForceType == 3:
          F[iatom][0] = P*vec[0]
      elif SimPar.ExtraForceType == 4:
          F[iatom][1] = P*vec[1]
      elif SimPar.ExtraForceType == 5:
          F[iatom][2] = P*vec[2]
      
  return(F)

#==========================================================================================
def Vext(target,reference):
#==========================================================================================
  
    if SimPar.ExtraForceType == 0:
        print 'Vext was called, even though ExtraForceType is 0. Something wrong'
        raise
    elif SimPar.ExtraForceType == 1:
        returnVext = numpy.sum(numpy.multiply(SimPar.ExtraForce, reference.Coord - target.Coord))
    elif SimPar.ExtraForceType >= 2:
        # Path integral <grumble>
        drstep = 0.01
        Ntrap = int(numpy.ceil(numpy.amax(numpy.absolute(reference.Coord - target.Coord)) / drstep))
        print 'Integrating over Ntrap = ',Ntrap

        returnVext = 0.0
        dr = (reference.Coord - target.Coord)/Ntrap

        rleft = copy.deepcopy(target.Coord)
        Fleft = FHydro(SimPar.ExtraForcePressure, rleft)
        for i in range(Ntrap):
            rright = rleft+dr
            Fright = FHydro(SimPar.ExtraForcePressure, rright)

            # This is basically numpy.sum(numpy.dot( (Fright+Fleft),dr ) )
            # It complains that the matrices are not aligned, so, I'm doing it by hand.
            for iatom in range(SimPar.Natom):
                for j in range(3):
                    returnVext = returnVext + 0.5*((Fright[iatom][j]+Fleft[iatom][j])*dr[iatom][j])

            rleft = copy.deepcopy(rright)
            Fleft = copy.deepcopy(Fright)
    
    return(returnVext)
  
