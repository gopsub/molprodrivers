#!/usr/bin/python

import argparse
import copy
import SimPar
from FileUtility import *
from PESUtility import * 
import AbInit
import ExtraForceUtility
import shutil
import vecutils

## Program that uses the NEB machinery to perform a dimer search


parser = argparse.ArgumentParser()
parser.add_argument('CaseName'    ,help='Prefix for all output and input files')
parser.add_argument('--SleepTime' ,help='Time in seconds to sleep before checking on Ab Initio jobs. Default=0.1',type=float,default=0.1)
args = parser.parse_args()

# Copy shit from command line arguments into relevant variables
CaseName         = copy.deepcopy(args.CaseName)
SimPar.SleepTime = copy.deepcopy(args.SleepTime)

print 'WARNING: DIMER IS A WORK IN PROGRESS'
print 'WARNING: DIMER IS A WORK IN PROGRESS'
print 'WARNING: DIMER IS A WORK IN PROGRESS'
print 'WARNING: DIMER IS A WORK IN PROGRESS'
print 'WARNING: DIMER IS A WORK IN PROGRESS'
print 'Hit enter to continue'
raw_input()

try:
    ifile = open(CaseName+'.input','r')
except:
    print '------> Cannot open input file <------'
    raise

AbInit.Software       = ifile.readline().partition('#')[0].strip().upper()
iseed                 = int(ifile.readline().partition('#')[0])
SimPar.Nactive        = int(ifile.readline().partition('#')[0])
SimPar.ExtraForceType = int(ifile.readline().partition('#')[0])
MinimizeEndpoint      = ifile.readline().partition('#')[0].strip().upper()
Nitermax              = int(ifile.readline().partition('#')[0])
dimerdR               = float(ifile.readline().partition('#')[0])
gfac                  = float(ifile.readline().partition('#')[0])
drcrit                = float(ifile.readline().partition('#')[0])
dEcrit                = float(ifile.readline().partition('#')[0])
Fcrit                 = float(ifile.readline().partition('#')[0])
ifile.close()


# Hard-wired parameters, so we can use the NEB machinery
# Also, seed RNG
SimPar.MaxJobs = 2
Nimage         = 2
numpy.random.seed(iseed)


#######################################
# Read in Ab Initio Software parameters
#######################################
if   AbInit.Software == 'MOLPRO':
    AbInit.MolPro_Header, AbInit.MolPro_OtherOptions, AbInit.MolPro_Basis, AbInit.MolPro_Method = readMolPro(CaseName+'.input')
elif AbInit.Software == 'GAMESS':
    AbInit.GAMESS_Header = readGAMESS(CaseName+'.input')
  
######################################
# SECRET PARAMETERS, and Overrides
######################################
print '--------------------------------------------------'
print ' STARTING DIMER SEARCH WITH THE FOLLOWING PARAMETERS'
print '    Case Name         = ',CaseName
print '    Active atoms      = ',SimPar.Nactive
print '    Extra Forces      = ',SimPar.ExtraForceType
print '    iseed             = ',iseed
print '    MinimizeEndpoint  = ',MinimizeEndpoint
print '    Nitermax          = ',Nitermax
print '    dimerdR           = ',dimerdR
print '    gfac              = ',gfac
print '    drcrit            = ',drcrit
print '    dEcrit            = ',dEcrit
print '    Fcrit             = ',Fcrit
print '--------------------------------------------------'
print ' AB INITIO PARAMETERS'
print '    Software = ',AbInit.Software
if AbInit.Software == 'MOLPRO':
    print '    Header   = ',AbInit.MolPro_Header
    print '    Others   = ',AbInit.MolPro_OtherOptions
    print '    Basis    = ',AbInit.MolPro_Basis
    print '    Method   = \n',AbInit.MolPro_Method
elif AbInit.Software == 'GAMESS':
    print '    HEADER   = ',AbInit.GAMESS_Header
  
  
######################################
# Preliminary checks on input data
######################################
# Checks that will make the code stop
if AbInit.Software == 'GAMESS':
    print 'Hello Polly -- GAMESS Code under development'
    sys.exit()
  
if AbInit.Software != 'MOLPRO' and AbInit.Software != 'GAMESS':
    print '------> Invalid Software choice. <------'
    print '  Valid choices are :'
    print '   MolPro'
    print '   GAMESS'
    raise
if SimPar.ExtraForceType > 5 or SimPar.ExtraForceType<0 :
    print '------> Invalid ExtraForceType. <------'
    print '  Valid choices are :'
    print '  0 = No extra force'
    print '  1 = Constant'
    print '  2 = Hydrostatic'
    print '  3 = X-only'
    print '  4 = Y-only'
    print '  5 = Z-only'
    raise

# Checks that will raise warnings

######################################
# Create Work Directories
######################################
WorkDir = [None]*Nimage
for img in range(Nimage):
    WorkDir[img] = CaseName+'.image.'+str(img)
    if os.path.exists(WorkDir[img]):
        print '------> Working directories from a previous simulation exist <------'
        raise
    os.mkdir(WorkDir[img])


########################################################################
# Read in config from file, and set it as reference
########################################################################
config       = [None]*Nimage
config[0]    = readXYZ(CaseName+'.start')

#######################################
# Find Natom, and set DOFMask
#######################################
SimPar.Natom   = config[0].Natom
SimPar.setDOFMask()



######################################
# Create arrays for additional forces (in Ha/Bohr)
######################################
ExtraForceUtility.Initialize(CaseName)


######################################
# Minimize start config, if requested
######################################
if AbInit.MolPro_Method == 'hf':
    NewMethod = 'hf'
else:
    NewMethod = 'hf\n'+AbInit.MolPro_Method
  
if MinimizeEndpoint == "START":
    print 'Minimizing start with no force, using native minimizer'
    NativeMinimize([config[0]], [WorkDir[0]])
    writeXYZ(config[0],CaseName+'.start.minimized.unforced')
else:
    print 'Getting the energies of the start point'
    CreateInput([config[0]],[WorkDir[0]],NewMethod)
    ExecAbInit([WorkDir[0]])
    ReadOutputEnergy([config[0]],[WorkDir[0]])

######################################
# Copy the minimized config into the reference config
######################################
RefConfig    = copy.deepcopy(config[0]);

print 'WARNING WARNING WARNING: NEED  TO MINIMIZE WITH EXTRA FORCES'
print 'WARNING WARNING WARNING: NEED  TO MINIMIZE WITH EXTRA FORCES'
print 'WARNING WARNING WARNING: NEED  TO MINIMIZE WITH EXTRA FORCES'

######################################
# Copy files from image.0 directory
# and create a dimer, but respect
# DOFMask
######################################
for f in ['integ','wfunc']:
    shutil.copyfile(WorkDir[0]+'/molpro.'+f, WorkDir[1]+'/molpro.'+f)
    
addthis                                = 2*numpy.random.rand(SimPar.Natom,3) - 1
addthis[numpy.invert(SimPar.DOFMask)]  = 0
addthis                                = dimerdR*vecutils.vunit(addthis)
config[0].Coord                       += addthis
config[1]                              = copy.deepcopy(config[0])
config[1].Coord                       += dimerdR* vecutils.vunit( addthis )


######################################
# Start moving dimer
######################################
Niter  = 0
drstep = [None]*Nimage
Rmid   = copy.deepcopy(config[0])
while True:

    if (Niter == 0) and (AbInit.MolPro_Method != 'hf'):
        NewMethod = 'hf\n'+AbInit.MolPro_Method
    else:
        NewMethod = AbInit.MolPro_Method
    NewMethod = NewMethod+'\nforce;varsav;table,gradx,grady,gradz;format,\'d45.35,d45.35,d45.35\''

    ###################################
    # Copy old shit
    ###################################
    Lastdrstep   = copy.deepcopy(drstep)
    Lastconfig   = copy.deepcopy(config)
    
    
    ###################################
    # Force call
    ###################################
    CreateInput(config,WorkDir,NewMethod)
    ExecAbInit(WorkDir)
    ReadOutputEnergy  (config,WorkDir)
    ReadOutputGradient(config,WorkDir)
    

    ###################################
    # Add forces, if needed WARNING -- NEEDS CHECKING
    ###################################
    if SimPar.ExtraForceType==1:
        print 'Adding constant forces'
        for iimg in range(Nimage):
          config[iimg].Gradient = config[iimg].Gradient - SimPar.ExtraForce
          config[iimg].E        = config[iimg].E + ExtraForceUtility.Vext(config[iimg],RefConfig)

    elif SimPar.ExtraForceType >= 2:
        print 'Adding Hydrostatic Forces'
        for iimg in range(Nimage):
          config[iimg].Gradient = config[iimg].Gradient - ExtraForceUtility.FHydro(SimPar.ExtraForcePressure,config[iimg].Coord) 
          config[iimg].E        = config[iimg].E + ExtraForceUtility.Vext(config[iimg],RefConfig)

    
    ###################################
    # Calculate dimer vector, midpoint, 
    # and force projections
    ###################################
    dRvec      = config[1].Coord - config[0].Coord
    dRhat      = vecutils.vunit(dRvec)
    Rmid.Coord = 0.5*(config[0].Coord + config[1].Coord)
    F          = [None]*Nimage
    Fperp      = [None]*Nimage
    Fparl      = [None]*Nimage
    for i in range(Nimage):
        F[i]     = -copy.deepcopy(config[i].Gradient)
        Fparl[i] = vecutils.vecproj(F[i], dRhat)
        Fperp[i] = F[i] - Fparl[i]

    Fnet     = F[0]     + F[1]
    Fnetparl = Fparl[0] + Fparl[1]
    Frot     = Fperp[0] - Fperp[1]
    Edimer   = config[0].E + config[1].E
    Fsign    = numpy.sign(numpy.vdot(F[0],F[1]))


    ###################################
    # Check for termination
    ###################################
    Terminate = False
    Fmaxparl  = numpy.amax(abs(Fnetparl[SimPar.DOFMask]))
    Fmaxrot   = numpy.amax(abs(Frot    [SimPar.DOFMask]))
    Fmaxnet   = numpy.amax(abs(Fnet    [SimPar.DOFMask]))
    Fmax      = max(Fmaxparl, Fmaxrot, Fmaxnet)
    if Niter != 0:
        if Fmax < Fcrit:
            Terminate = True
        if Niter > Nitermax:
            Terminate = True
            

    ###################################
    # Print stuff, and write to file
    ###################################
    print
    print '-------------------'
    print 'Niter = ',Niter
    print 'Fmaxparl, Fmaxrot, Fmaxnet, Edimer = ',Niter,Fmaxparl, Fmaxrot, Fmaxnet, Edimer
    print 'Fsign = ',Fsign
    
    appendXYZ(Rmid,CaseName+'.dimer')
    appendXYZ(config[0],CaseName+'.dimer.0')


    if Terminate:
        break

    ###################################
    # Move images, the stupid way
    ###################################
    if Niter%2 != 0:
        print "Rotating"
        drstep[0] = gfac*Frot
        drstep[1] = -drstep[0]
    else:
        print "Translating"
        drstep[0] = gfac*(Fnet - 2*Fnetparl)
        drstep[1] = gfac*(Fnet - 2*Fnetparl)


    # Limit the drstep, and move the buggers
    for iimg in range(Nimage):
        mask                               = abs(drstep[iimg]) > drcrit
        drstep[iimg][mask]                 = drcrit*numpy.sign(drstep[iimg][mask])
        config[iimg].Coord[SimPar.DOFMask] = config[iimg].Coord[SimPar.DOFMask] + drstep[iimg][SimPar.DOFMask]

    #Pull the new images back, to maintain the same dimerdR
    Rmidnew  = 0.5*(config[0].Coord + config[1].Coord)
    dRnew    = config[1].Coord - config[0].Coord
    dRnewhat = vecutils.vunit(dRnew)
    config[0].Coord = Rmidnew - 0.5*dimerdR*dRnewhat
    config[1].Coord = Rmidnew + 0.5*dimerdR*dRnewhat

    



    Niter += 1


print "End of dimer search"
    
