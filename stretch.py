#!/usr/bin/python

import argparse
import copy
import AbInit, SimPar
from FileUtility import *
from PESUtility import * 
import xyz


# Calculate the force-extension curve of a molecule, using much of the NEB machinery for setting up directories,
# 
#
# Details:
# 0. Minimize the start config, with all atoms moving
# 1. Last atom is moved to the origin, and kept there for the entirety of the simulation.
# 2. Molecule is rotated, so that the second-to-last atom is on the X-axis
# 3. The second-to-last atom is moved by a small amount dx along the X-axis
# 4. The structure is minimized, with the last 2 atoms held fixed.
# 5. Energy and forces are calculated, and written to file.
# 6. Repeat from step 3, until NiterMax is reached.
#

parser = argparse.ArgumentParser()
parser.add_argument('CaseName'    ,help='Prefix for all output and input files')
parser.add_argument('--SleepTime' ,help='Time in seconds to sleep before checking on Ab Initio jobs. Default=0.1',type=float,default=0.1)
args = parser.parse_args()

# Hard-wired parameters, so we can use the NEB machinery
SimPar.MaxJobs = 1
Nimage         = 1


# Copy command line arguments into relevant variables. Hardwire a few
CaseName         = copy.deepcopy(args.CaseName)
SimPar.SleepTime = copy.deepcopy(args.SleepTime)

try:
    ifile = open(CaseName+'.input','r')
except:
    print '------> Cannot open input file <------'
    raise

dx               = float(ifile.readline().partition('#')[0])
drcrit           = float(ifile.readline().partition('#')[0])
dEcrit           = float(ifile.readline().partition('#')[0])
Fcrit            = float(ifile.readline().partition('#')[0])
Nitermax         = int(ifile.readline().partition('#')[0])
MinimizeEndpoint = ifile.readline().partition('#')[0].strip().upper()
ifile.close()


#######################################
# Read in Ab Initio Software parameters
#######################################
AbInit.Software       = 'MOLPRO'
if   AbInit.Software == 'MOLPRO':
    AbInit.MolPro_Header, AbInit.MolPro_OtherOptions, AbInit.MolPro_Basis, AbInit.MolPro_Method = readMolPro(CaseName+'.input')
elif AbInit.Software == 'GAMESS':
    AbInit.GAMESS_Header = readGAMESS(CaseName+'.input')
  
  

  
######################################
# Create Work Directories
######################################
WorkDir = [None]*Nimage
for img in range(Nimage):
    WorkDir[img] = CaseName+'.image.'+str(img)
    if os.path.exists(WorkDir[img]):
        print '------> Working directories from a previous simulation exist <------'
        raise
    os.mkdir(WorkDir[img])
 
   
#######################################
# Read in initial config, and set DOF Mask to all atoms
#######################################
config       = [None]*Nimage
config[0]    = readXYZ(CaseName+'.start')
SimPar.Natom = copy.deepcopy(config[0].Natom)


SimPar.Nactive=-1
SimPar.setDOFMask()



######################################
# Create array for additional forces (in Ha/Bohr)
######################################
ExtraForceUtility.Initialize(CaseName)


######################################
# Minimize start config, if requested
######################################
if AbInit.MolPro_Method == 'hf':
    NewMethod = 'hf'
else:
    NewMethod = 'hf\n'+AbInit.MolPro_Method
  
if MinimizeEndpoint == "START":
    print 'Minimizing start with no force, using native minimizer'
    NativeMinimize([config[0]], [WorkDir[0]])
    writeXYZ(config[0],CaseName+'.start.minimized.unforced')
else:
    print 'Getting the energies of the start point'
    CreateInput([config[0]],[WorkDir[0]],NewMethod)
    ExecAbInit([WorkDir[0]])
    ReadOutputEnergy([config[0]],[WorkDir[0]])
    
  
#######################################
# Translate and Rotate molecule, 
# so that last atom 
# is at the origin, and second-to-last 
# is on the X-axis
#######################################
xyz.MoveToOrigin(config[0],-1)
v = config[0].Coord[-2] - config[0].Coord[-1]
xyz.RotateVectorToDirection(config[0],v,numpy.array([1,0,0]))


#######################################
# Redefine DOFMask so that last two atoms are not moved
#######################################
SimPar.Nactive = SimPar.Natom - 2
SimPar.setDOFMask()


#######################################
# Open files for output
#######################################

resultsfile = open('results','w')
results_format='%d   %f   %f   %f\n' 
resultsfile.write('#Iteration,   Distance (Ang),   Force (Ha/Bohr),   Energy (Ha) \n')
resultsfile.write(results_format %(0, config[0].Coord[-2][0]-config[0].Coord[-1][0], 0.0, config[0].E))
xyz.writeXYZ(config[0],'results.xyz')
NewMethod = NewMethod+'\nforce;varsav;table,gradx,grady,gradz;format,\'d45.35,d45.35,d45.35\''


#######################################
# And finally, stretch the bugger
#######################################

for i in range(Nitermax):
    print
    print "STRETCHING, Step ",i
    config[0].Coord[-2][0] += dx
    NativeMinimize(config, WorkDir)

    # Force call
    CreateInput(config,WorkDir,NewMethod)
    ExecAbInit(WorkDir)
  
    # Read energies, and gradients
    ReadOutputEnergy  (config,WorkDir)
    ReadOutputGradient(config,WorkDir)

    resultsfile.write(results_format %(i+1, config[0].Coord[-2][0]-config[0].Coord[-1][0], config[0].Gradient[-1][0], config[0].E))
    appendXYZ(config[0],'results.xyz')
    resultsfile.flush()
    

    
