# Global variables for simulation parameters. Set to default values
Natom   = -1
Nactive = -1
DOFMask = []
#------------------------------------
MaxJobs   = 7
SleepTime = 0.1
#------------------------------------
ExtraForceType     = 0
ExtraForce         = []
ExtraForcePressure = 0.0

import numpy
import SimPar
def setDOFMask():
    SimPar.DOFMask = (numpy.zeros((SimPar.Natom,3)) + 1) < 0 # Creates an array of all False
    if SimPar.Nactive == -1 or SimPar.Nactive == -2:
        SimPar.DOFMask = True+SimPar.DOFMask
    elif SimPar.Nactive > 0:
        for iatom in range(SimPar.Nactive):
            for j in range(3):
                SimPar.DOFMask[iatom][j] = True
    elif SimPar.Nactive == 0:
        print '------> Reading DOFmask from file not coded yet. <------'
        sys.exit()
    else:
        print '------> Invalid number of Nactive <------'
        sys.exit()
