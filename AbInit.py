# Global Variables for Ab Initio calculation. Set to the default values
Software    = 'MOLPRO'

#-------------------------------------------
MolPro_Basis       ='cc-pvdz'
MolPro_Method      ='hf;mp2'
MolPro_OtherOptions=[]
MolPro_Header      =[]

#-------------------------------------------
GAMESS_Header = ' $SYSTEM mwords=125 $END \n $BASIS  gbasis=am1 $END\n $CONTRL scftyp=RHF $END'
