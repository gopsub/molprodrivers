#!/usr/bin/python

import argparse
import copy
import SimPar
from FileUtility import *
from PESUtility import * 
import AbInit
import ExtraForceUtility


## Program that uses the NEB machinery to run MD on molecules.
## 1. NVE or NVT dynamics are possible
## 2. In either case, the system is thermalized for ttherm
## 3. After thermalization, the requested type of dynamics is run
## 4. After thermalization, forces and positions are output every timestep
## 5. Configuration is output every Nprint steps


parser = argparse.ArgumentParser()
parser.add_argument('CaseName'    ,help='Prefix for all output and input files')
parser.add_argument('--SleepTime' ,help='Time in seconds to sleep before checking on Ab Initio jobs. Default=0.1',type=float,default=0.1)
args = parser.parse_args()

# Copy shit from command line arguments into relevant variables
CaseName         = copy.deepcopy(args.CaseName)
SimPar.SleepTime = copy.deepcopy(args.SleepTime)


try:
    ifile = open(CaseName+'.input','r')
except:
    print '------> Cannot open input file <------'
    raise

AbInit.Software       = ifile.readline().partition('#')[0].strip().upper()
dynType               = ifile.readline().partition('#')[0].strip().upper()
T                     = float(ifile.readline().partition('#')[0])
gamma                 = float(ifile.readline().partition('#')[0])
dt                    = float(ifile.readline().partition('#')[0])
tMD                   = float(ifile.readline().partition('#')[0])
ttherm                = float(ifile.readline().partition('#')[0])
Nprint                = int(ifile.readline().partition('#')[0])
iseed                 = int(ifile.readline().partition('#')[0])
SimPar.Nactive        = int(ifile.readline().partition('#')[0])
SimPar.ExtraForceType = int(ifile.readline().partition('#')[0])
ifile.close()

## Hardwired numbers, and seed RNG
BOLTZ  = 1.3806488E-23
Nimage = 1
numpy.random.seed(iseed)


#######################################
# Read in Ab Initio Software parameters
#######################################
if   AbInit.Software == 'MOLPRO':
    AbInit.MolPro_Header, AbInit.MolPro_OtherOptions, AbInit.MolPro_Basis, AbInit.MolPro_Method = readMolPro(CaseName+'.input')
elif AbInit.Software == 'GAMESS':
    AbInit.GAMESS_Header = readGAMESS(CaseName+'.input')
  
######################################
# SECRET PARAMETERS, and Overrides
######################################
print '--------------------------------------------------'
print ' STARTING MD WITH THE FOLLOWING INPUT PARAMETERS'
print '    Case Name         = ',CaseName
print '    Dynamics type     = ',dynType
print '    Active atoms      = ',SimPar.Nactive
print '    Extra Forces      = ',SimPar.ExtraForceType
print '    Temperature       = ',T
print '    Gamma             = ',gamma
print '    iseed             = ',iseed
print '    ttherm            = ',ttherm
print '    tMD               = ',tMD
print '--------------------------------------------------'
print ' AB INITIO PARAMETERS'
print '    Software = ',AbInit.Software
if AbInit.Software == 'MOLPRO':
    print '    Header   = ',AbInit.MolPro_Header
    print '    Others   = ',AbInit.MolPro_OtherOptions
    print '    Basis    = ',AbInit.MolPro_Basis
    print '    Method   = \n',AbInit.MolPro_Method
elif AbInit.Software == 'GAMESS':
    print '    HEADER   = ',AbInit.GAMESS_Header
  
  
######################################
# Preliminary checks on input data
######################################
# Checks that will make the code stop
if AbInit.Software == 'GAMESS':
    print 'Hello Polly -- GAMESS Code under development'
    sys.exit()
  
if AbInit.Software != 'MOLPRO' and AbInit.Software != 'GAMESS':
    print '------> Invalid Software choice. <------'
    print '  Valid choices are :'
    print '   MolPro'
    print '   GAMESS'
    raise
if SimPar.ExtraForceType > 5 or SimPar.ExtraForceType<0 :
    print '------> Invalid ExtraForceType. <------'
    print '  Valid choices are :'
    print '  0 = No extra force'
    print '  1 = Constant'
    print '  2 = Hydrostatic'
    print '  3 = X-only'
    print '  4 = Y-only'
    print '  5 = Z-only'
    raise

# Checks that will raise warnings

######################################
# Create Work Directories
######################################
WorkDir = [None]*Nimage
for img in range(Nimage):
    WorkDir[img] = CaseName+'.image.'+str(img)
    if os.path.exists(WorkDir[img]):
        print '------> Working directories from a previous simulation exist <------'
        raise
    os.mkdir(WorkDir[img])
WorkDir = [CaseName+'.image.0']


########################################################################
# Read in config from file, and set it as reference
########################################################################
config       = [None]*1
config[0]    = readXYZ(CaseName+'.start')
RefConfig    = copy.deepcopy(config[0]);

#######################################
# Find Natom, and set DOFMask
#######################################
SimPar.Natom   = config[0].Natom
SimPar.setDOFMask()



######################################
# Create arrays for additional forces (in Ha/Bohr)
# masses, velocities
######################################
ExtraForceUtility.Initialize(CaseName)
Mass     = [None]*Nimage
Velocity = [None]*Nimage
for iconf in range(Nimage):
    Mass[iconf]     = CreateMassArray(config[iconf].ChemSym)*(1e-3/6.023e23) # Conversion to kilograms
    Velocity[iconf] = numpy.random.normal(0,1,(SimPar.Natom,3))
    Velocity[iconf] = numpy.multiply( numpy.sqrt(BOLTZ*T/Mass[0]), Velocity[iconf] )
      


######################################
# Open files for data
######################################
ForceFile    = open(CaseName+'.forces','w')
VelocityFile = open(CaseName+'.velocities','w')

######################################
# Start MD
######################################
Niter     = 0
drstep    = [None]*Nimage
time      = 0



while time < tMD+ttherm:

    print

    ## Copy old shit
    LastConfig   = copy.deepcopy(config)
    LastVelocity = copy.deepcopy(Velocity)

    ############################################################
    # Verlet step position update, but only if Niter > 0
    if Niter > 0:
        for iimg in range(Nimage):
            drstep                             = LastVelocity[iimg] * dt - 0.5*dt*dt*LastConfig[iimg].Gradient/Mass[iimg]
            drstep                             = 1e10*drstep # Convert drstep into angstroms
            config[iimg].Coord[SimPar.DOFMask] = LastConfig[iimg].Coord[SimPar.DOFMask] + drstep[SimPar.DOFMask]


    ## Find forces
    if (Niter == 0) and (AbInit.MolPro_Method != 'hf'):
        NewMethod = 'hf\n'+AbInit.MolPro_Method
    else:
        NewMethod = AbInit.MolPro_Method
    NewMethod = NewMethod+'\nforce;varsav;table,gradx,grady,gradz;format,\'d45.35,d45.35,d45.35\''

    #####  This bit gets the forces and energies.
    #Create Input files for everyone
    for iimg in range(Nimage):
        CreateInput([config[iimg]],[WorkDir[iimg]],NewMethod)

    # Run AbInit
    ExecAbInit(WorkDir)

    # Read energies, and gradients
    ReadOutputEnergy  (config,WorkDir)
    ReadOutputGradient(config,WorkDir)

    # Add forces, if needed
    if SimPar.ExtraForceType==1:
        for iimg in range(Nimage):
            config[iimg].Gradient = config[iimg].Gradient - SimPar.ExtraForce
            config[iimg].E        = config[iimg].E + ExtraForceUtility.Vext(config[iimg],RefConfig)

    elif SimPar.ExtraForceType >= 2:
        for iimg in range(Nimage):
            config[iimg].Gradient = config[iimg].Gradient - ExtraForceUtility.FHydro(SimPar.ExtraForcePressure,config[iimg].Coord) 
            config[iimg].E        = config[iimg].E + ExtraForceUtility.Vext(config[iimg],RefConfig)


    # Convert force from Ha/Bohr into Newton
    print 'Niter, E (Ha) = ',Niter,config[0].E,
    if time <= ttherm:
        print '   THERMALIZING'
    else:
        print
    for iimg in range(Nimage):
        config[iimg].Gradient = config[iimg].Gradient*(4.35974434e-8/0.5291)


    ############################################################
    # Thermostat the atoms (Note -- Thermostatting is done in SI units)
    ############################################################
    if time < ttherm or dynType == 'NVT':
        for iimg in range(Nimage):
            F_diss = - gamma * Mass[iimg] * LastVelocity[iimg]
            F_rand = numpy.sqrt(2*gamma*BOLTZ*T*Mass[iimg]/dt)*numpy.random.normal(0,1,(SimPar.Natom,3))
            config[iimg].Gradient = config[iimg].Gradient - F_diss
            config[iimg].Gradient = config[iimg].Gradient - F_rand


    ############################################################
    # Verlet velocity update, but only if Niter > 0
    if Niter > 0:
        for iimg in range(Nimage):
            Velocity[iimg] = LastVelocity[iimg] - 0.5*dt*( config[iimg].Gradient + LastConfig[iimg].Gradient )/Mass[iimg]


    ## Write snapshot out to file
    if Niter%Nprint == 0:
        if time <= ttherm:
            appendXYZ(config[0],CaseName+'.thermalizing.xyz','t = %20.12e' %(time))
        else:
            appendXYZ(config[0],CaseName+'.trajectory.xyz','t = %20.12e' %(time))
            

    ## Output forces and velocities to file
    if time > ttherm:
        for ii in range(SimPar.Natom):
            for jj in range(3):
                ForceFile.write   ('%30.20e ' %(-config[0].Gradient[ii][jj]))
                VelocityFile.write('%30.20e ' %(Velocity[0][ii][jj]))
        ForceFile.write('\n')
        VelocityFile.write('\n')



    ### Update iterations counter
    Niter += 1
    time  += dt

### END OF LOOP ###


