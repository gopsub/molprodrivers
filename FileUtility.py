from ClassDefinition import *
import SimPar, AbInit

import string
import os
import sys

#====================================================================================
def readGAMESS(filename):
   
#Read GAMESS parameters from input file
#   
#====================================================================================
    print 'Reading in GAMESS-Parameters from input file'
    try:
      ifile=open(filename,'r')
    except:
      print '---> Trying to read GAMESS-Parameters. Input file cannot be opened <---'

    Header = ''
    while True:
        line = ifile.readline().partition('#')[0].strip()
        if line == 'GAMESS-PARAMETERS':
            while True:
              l = ifile.readline().partition('#')[0].strip()
              if l == 'END-GAMESS-PARAMETERS':
                break
              else:
                Header = Header + '\n' + ' ' + l


            print 'GAMESS Header read in:'
            print Header

            break

    ifile.close()
    return Header

#====================================================================================
def readMolPro(filename):
#
# Read MolPro parameters from input file
#====================================================================================
    print 'Reading in MolPro-Parameters from input file'
    try:
        ifile=open(filename,'r')
    except:
        print '---> Trying to read MolPro-Parameters. Input file cannot be opened <---'

    while True:
        line = ifile.readline().partition('#')[0].strip()
        if line == 'MOLPRO-PARAMETERS':
            Header       = ifile.readline().partition('#')[0].strip()
            OtherOptions = ifile.readline().partition('#')[0].strip()
            Basis        = ifile.readline().partition('#')[0].strip()
            Method       = ifile.readline().partition('#')[0].strip()

            print 'MolPro Parameters read in:'
            print ' Header        = ',Header
            print ' Other Options = ',OtherOptions
            print ' Basis         = ',Basis
            print ' Method        = ',Method
            print

            break

    ifile.close()
    return Header, OtherOptions, Basis, Method

#====================================================================================
def readFIRE(filename):
#
# Read parameters fro FIRE minimizer
#====================================================================================
    print 'Reading in FIRE-PARAMETERS from input file'
    try:
        ifile=open(filename,'r')
    except:
        print '---> Trying to read FIRE parameters. Input file disappeared <---'

    #Go through the file, until we get 'FIRE-PARAMETERS'. Then read in parameters.
    while True:
        line=ifile.readline().partition('#')[0].strip()
        if line == 'FIRE-PARAMETERS':
            alpha_start =float(ifile.readline().partition('#')[0].strip())
            dt_start    =float(ifile.readline().partition('#')[0].strip())
            dt_max      =float(ifile.readline().partition('#')[0].strip())
            fdec_dt     =float(ifile.readline().partition('#')[0].strip())
            finc_dt     =float(ifile.readline().partition('#')[0].strip())
            falpha      =float(ifile.readline().partition('#')[0].strip())
            Nadjustmin  =int(ifile.readline().partition('#')[0].strip())

            print 'Fire Parameters read in:'
            print 'alpha_start = ',alpha_start
            print 'dt_start    = ',dt_start
            print 'dt_max      = ',dt_max
            print 'fdec_dt     = ',fdec_dt
            print 'finc_dt     = ',finc_dt
            print 'falpha      = ',falpha
            print

            break

    ifile.close()
    return alpha_start, dt_start, dt_max, fdec_dt, finc_dt, falpha, Nadjustmin
       

       
#====================================================================================
def readQM(filename):
#
# Read parameters for QuickMin minimizer
#====================================================================================
    print 'Reading in QM-PARAMETERS from input file'
    try:
        ifile=open(filename,'r')
    except:
        print '---> Trying to read QM parameters. Input file disappeared <---'

    #Go through the file, until we get 'FIRE-PARAMETERS'. Then read in parameters.
    while True:
        line=ifile.readline().partition('#')[0].strip()
        if line == 'QM-PARAMETERS':
            dt =float(ifile.readline().partition('#')[0].strip())

            print 'QM Parameters read in:'
            print 'dt = ',dt
            break

    ifile.close()
    return dt
       

#====================================================================================
def WriteVector(filename, v, header='#Default header'):
#
# Write out a vector to file
#====================================================================================
    n = v.shape[0]
    ofile = open(filename,'w')
    ofile.write('# '+header.strip()+'\n')
    ofile.write('%d\n' %(n))
    for i in range(n):
        ofile.write('%e \n' %(v[i]))
    ofile.close()
#====================================================================================
def WriteSquareMatrix(filename, H, header='#Default header'):
#
# Write a square matrix to file
#====================================================================================
    n = H.shape[0]
    if H.shape[0] != n:
        print '---> Attempt to print a non-square matrix <---'
        raise

    ofile = open(filename,'w')
    ofile.write('# '+header.strip()+'\n')
    ofile.write('%d\n' %(n))
    for i in range(n):
        for j in range(n):
            ofile.write('%e ' %(H[i][j]))
        ofile.write('\n')
    ofile.close()

#====================================================================================
def CreateInput(config, WorkDir, NewMethod = ''):
#
# Routine to create a MolPro input file.
# Needs:
#  1. config    : List of ConfigClass
#  2. WorkDir   : List of Working Directories
#  3. NewMethod : In you want to override the Method set in the AbInit module, pass the alternative in here
#====================================================================================

    if len(NewMethod) == 0:
        NewMethod = AbInit.MolPro_Method
      
    NConfig = len(config)
    for iconfig in range(NConfig):
        ofile = open(WorkDir[iconfig]+'/molpro.inp','w')
        ofile.write('!\n!AUTOMATICALLY CREATED MOLPRO INPUT FILE\n!\n')
        ofile.write(AbInit.MolPro_Header+'\n\n')
        ofile.write('nosym\n');
        ofile.write('basis='+AbInit.MolPro_Basis+'\n\n')
        ofile.writelines(AbInit.MolPro_OtherOptions+'\n')
        ofile.write('file,1,molpro.integ\n')
        ofile.write('file,2,molpro.wfunc\n\n\n')
        WriteMolProGeometry(config[iconfig],ofile)
        ofile.writelines(NewMethod)
        ofile.write('\n\n')
        ofile.close()
      
#========================================================================================================  
def ReadHess(WorkDir):
#========================================================================================================  
    H = [None]*len(WorkDir)
    for iconf in range(len(WorkDir)):
        H[iconf] = numpy.zeros((3*SimPar.Natom, 3*SimPar.Natom),dtype=float)

        HessFound = False
        with open(WorkDir[iconf]+'/molpro.out','r') as ifile:
            while True:
                line = ifile.readline()
                if not line:
                    break
                if len(line.split())<4:
                    continue
                if line.split()[0] == 'Force' and line.split()[1] == 'Constants' and line.split()[2] == '(Second' and line.split()[3] == 'Derivatives':
                    print 'HESS BLOCK FOUND'
                    HessFound = True
                    break

            line=ifile.readline()
            lines2read=3*SimPar.Natom
            rowstart = 0
            while lines2read > 0:
                for iline in range(lines2read):
                    vals = numpy.array(ifile.readline().split()[1:],dtype=float)
                    row = rowstart + iline
                    colstart = rowstart
                    colend   = colstart + len(vals)
                    H[iconf][row][colstart:colend] = copy.deepcopy(vals)

                ifile.readline()
                lines2read -=5
                rowstart +=5


        H[iconf] = H[iconf] + numpy.transpose(H[iconf])
        for i in range(3*SimPar.Natom):
           H[iconf][i][i] = 0.5*H[iconf][i][i]

    return(H)

#========================================================================================================
def ReadOutputGradient(config,WorkDir):
#
# Parse molpro output to get gradients
# Gradients are in Ha/Bohr
#========================================================================================================
    for iconf in range(len(config)):
        try:
            ifile = open(WorkDir[iconf]+'/molpro.out','r')
        except:
            print '------> Unable to open file to read MolPro Output <------'
            raise

        GradientFound = False
        while True:
            line = ifile.readline()
            if not line:
                break

            if len(line.split()) < 3:
                continue

            if line.split()[0] == 'GRADX' and line.split()[1] == 'GRADY' and line.split()[2] == 'GRADZ':
                GradientFound = True
                for iatom in range(SimPar.Natom):
                    line = ifile.readline().split()
                    for j in range(3):
                        config[iconf].Gradient[iatom][j] = float(line[j].replace('D','E'))

            #Wait--don't break yet. There may be another gradient block.


        if not GradientFound:
            print '------> Gradient block not found in output file <------'
            raise


#========================================================================================================  
def ReadOutputEnergy(config,WorkDir):
#
# Parse molpro output to get energy
# I ASSUME THAT THE ENERGY IS IN THE FIRST COLUMN OF THE THIRD-LAST LINE OF THE OUTPUT FILE
# I also assume that Energy output by MolPro is Hartree
#========================================================================================================  

    for iconf in range(len(config)):
        os.system('tail -n3 '+WorkDir[iconf]+'/molpro.out | head -n1 >ENERGY_LINE')
        efile = open('ENERGY_LINE')
        config[iconf].E = float(efile.readline().split()[0])
        efile.close()
        os.remove('ENERGY_LINE')
      
#========================================================================================================  
def ReadOutputConfig(config,WorkDir):
#========================================================================================================  
    for iconf in range(len(config)):
        try:
            ifile = open(WorkDir[iconf]+'/molpro.out','r')
        except:
            print '------> Unable to open file to read MolPro Output <------'
            raise


        ConfigFound = False
        while True:
            line = ifile.readline()
            if not line:
                break
            if line.strip() == 'Current geometry (xyz format, in Angstrom)':
                ConfigFound = True
                line  = ifile.readline()
                Natom = int(ifile.readline())
                if Natom != config[iconf].Natom:
                    print '------> Natom in output file does not match config <------'
                    raise

                line = ifile.readline().split()[1]
                config[iconf].E = float(line[7:])

                for iatom in range(Natom):
                    line = ifile.readline().split()
                    if config[iconf].ChemSym[iatom] != line[0]:
                        print '------> Chemical symbol mismatch <------'
                        raise
                    for icoord in range(3):
                        config[iconf].Coord[iatom][icoord] = float(line[icoord+1])

              # Wait -- Don't break yet. There might be another config stored later on

        if not ConfigFound:
            print '------> Something wrong with MolPro output file in ',WorkDir[iconf]
            raise

        ifile.close()


      
#========================================================================================================  
def readXYZ(filename):
#  Routine that reads in XYZ file format.
#  Expected format is
#     First line         : Number of atoms
#     Second line        : Comment
#     Third line onwards : One line per atom, ChemSym X Y Z
#========================================================================================================  

    try:
        ifile=open(filename,'r')
    except:
        print '------> Cannot open ',filename,' <------'
        raise
    config = ConfigClass()

    config.Natom = int(ifile.readline().strip())
    sdum  = ifile.readline()
    for iatom in range(config.Natom):
        line = ifile.readline().split()
        config.ChemSym.append(line[0])
        config.Coord = numpy.append(config.Coord, [numpy.array(map(float,line[1:]))], axis=0)
    ifile.close()
    config.Gradient = 0*config.Coord
    config.E = 0
    return(config)
  
#========================================================================================================  
def WriteMolProGeometry(config,ofile):
#   Writes out the config as a MolPro readable geometry block
#   NOTE: File should already be open.
#========================================================================================================  
    ofile.write('\nangstrom\n')
    ofile.write('geometry = {\n')
    for iatom in range(config.Natom):
        ofile.write(config.ChemSym[iatom]+' ,, '),
        for j in range(3):
            ofile.write('_'+str(iatom)+'_'+str(j)+'   '),
        ofile.write('\n')
    ofile.write('}\n\n')
    for iatom in range(config.Natom):
        for j in range(3):
            ofile.write('_'+str(iatom)+'_'+str(j)+' = %45.35e ; ' %(config.Coord[iatom][j]) )
        ofile.write('\n')
    ofile.write('\n\n')
  
#========================================================================================================  
def appendXYZ(config,filename,comment='XYZ Format file, create by NEBber utils'):
#  Routine that appends a config in XYZ file format to a preexisting file
#  Output format is
#     First line         : Number of atoms
#     Second line        : Comment
#     Third line onwards : One line per atom, ChemSym X Y Z
#========================================================================================================  
    try:
        ofile = open(filename,'a')
    except:
        print '------> Cannot open file ',filename,' for writing <------'
        raise

    ofile.write('%d\n' %(config.Natom))
    ofile.write(comment+'\n')
    for iatom in range(config.Natom):
        ofile.write(config.ChemSym[iatom])
        for j in range(3):
            ofile.write('%45.35e ' %(config.Coord[iatom][j]))
        ofile.write('\n')
    ofile.close()
#========================================================================================================  
def writeXYZ(config,filename):
#  Routine that writes a config in XYZ file format.
#  Output format is
#     First line         : Number of atoms
#     Second line        : Comment
#     Third line onwards : One line per atom, ChemSym X Y Z
#========================================================================================================  
    try:
        ofile = open(filename,'w')
    except:
        print '------> Cannot open file ',filename,' for writing <------'
        raise

    ofile.write('%d\n' %(config.Natom))
    ofile.write('XYZ format file, created by Python NEBber\n')
    for iatom in range(config.Natom):
        ofile.write(config.ChemSym[iatom])
        for j in range(3):
            ofile.write('%45.35e ' %(config.Coord[iatom][j]))
        ofile.write('\n')
    ofile.close()

#========================================================================================================  
def writechain(chain, filename):
#========================================================================================================  
    try:
        ofile = open(filename,'w')
    except:
        print '------> Cannot open chain file ',filename,' for writing <------'
        raise

    Nimage = len(chain)
    for iimg in range(Nimage):
        ofile.write('%d\n' %(SimPar.Natom))
        ofile.write('Image Number %d\n' %(iimg))
        for iatom in range(SimPar.Natom):
            ofile.write(chain[iimg].ChemSym[iatom]),
            for j in range(3):
                ofile.write("%45.35e " %(chain[iimg].Coord[iatom][j]))
            ofile.write("\n")
    ofile.close()
  
#========================================================================================================  
def readchain(filename,Nimage):
#========================================================================================================  
    try:
        ifile  = open(filename,'r')
    except:
        print '------> Cannot open chain file ',filename,' for reading <------'
        raise

    returnconfig = [None]*Nimage
    for iimg in range(Nimage):
        returnconfig[iimg] = ConfigClass()
        Natom = int(ifile.readline())

        ldum = ifile.readline()
        for iatom in range(Natom):
            line = ifile.readline().split()
            returnconfig[iimg].ChemSym.append(line[0])
            returnconfig[iimg].Coord = numpy.append(returnconfig[iimg].Coord,[numpy.array(map(float,line[1:]))],axis=0)
        returnconfig[iimg].Gradient = 0*returnconfig[iimg].Coord
        returnconfig[iimg].Natom    = Natom
        returnconfig[iimg].E        = 0
    ifile.close()
    return(returnconfig)
    
  
