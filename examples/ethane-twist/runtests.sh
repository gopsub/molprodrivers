NC=12                              # Number of characters of variable value (input file)
DOTS=$(printf '.%.s' $(seq 1 $NC))


pad(){
	PADDED=$(printf "%-"$NC"s" $1)
}


makesubst(){
	sed -i "$LINENUM"s/^"$DOTS"/"$PADDED"/ stagedir/ethane.input
}


runsingletest(){
	despaced=$(echo ${badrunstack[@]}|sed 's/ /::/g')  # Variable containing run type
	echo -n RUNNING $despaced " ... "
	
	cp -r stagedir testrun
	cd testrun
	tic=$(date +%s)
	python /home/gss/oldhome/projects/python/molproDrivers/runneb.py ethane > OUTPUT
	toc=$(date +%s)
	wct=$(expr $toc - $tic )
	exitstatus=$?
	Niter=$(grep Iteration OUTPUT | tail -n1 | awk '{print $3}')
	Status=$(grep Status OUTPUT | awk '{print $2}')
	echo 
	cd ..
	echo DONE
	echo

	echo -n "$despaced" "    " >> REPORT
	echo "Niter=$Niter $status WCT(s)=$wct" >> REPORT
	if [[ $exitstatus != 0 || $Status != "CONVERGED" ]]; then
		mv testrun badrun-$despaced
	else
		rm -rf testrun
	fi
}

module load molpro
rm -rf testrun stagedir badrun*
rm -f  REPORT
cp -r  clean stagedir


#----------------------
# Create the stack
declare -a badrunstack

for minimizer in fire; do
	LINENUM=10
	pad $minimizer
	makesubst

	#Push
	badrunstack[${#badrunstack[@]}]="$minimizer"

	for serialinit in 0 1; do
		LINENUM=15
		pad $serialinit
		makesubst

		#Push
		badrunstack[${#badrunstack[@]}]="serialinit_$serialinit"

		#--------- Begin run
		runsingletest
		#---------- End run

		#Pop serial init
		unset badrunstack[${#badrunstack[@]}-1]
	done
	
		
	# Pop minimizer
	unset badrunstack[${#badrunstack[@]}-1]

done

	

