#!/usr/bin/python


import argparse
import copy
import SimPar
import AbInit
from FileUtility import *
import ExtraForceUtility
from PESUtility import * 
import vecutils as vec
import transform
import shutil

"""
import numpy
import os,sys
"""

parser = argparse.ArgumentParser()
parser.add_argument('CaseName'    ,help='Prefix for all output and input files')
parser.add_argument('--MaxJobs'   ,help='Maximum number of Ab Initio jobs. Set this equal to number of cores on the node. Default=7',type=int,default=7)
parser.add_argument('--SleepTime' ,help='Time in seconds to sleep before checking on Ab Initio jobs. Default=0.1',type=float,default=0.1)
args = parser.parse_args()

# Copy shit from command line arguments into relevant variables
CaseName         = copy.deepcopy(args.CaseName)
SimPar.MaxJobs   = copy.deepcopy(args.MaxJobs)
SimPar.SleepTime = copy.deepcopy(args.SleepTime)


try:
    ifile = open(CaseName+'.input','r')
except:
    print '------> Cannot open input file <------'
    raise

AbInit.Software       = ifile.readline().partition('#')[0].strip().upper()
Nimage                = int(ifile.readline().partition('#')[0])
kspring               = float(ifile.readline().partition('#')[0])
gfac                  = float(ifile.readline().partition('#')[0])
drcrit                = float(ifile.readline().partition('#')[0])
dEcrit                = float(ifile.readline().partition('#')[0])
Fcrit                 = float(ifile.readline().partition('#')[0])
Nitermax              = int(ifile.readline().partition('#')[0])
ClimbingImage         = int(ifile.readline().partition('#')[0])
NEBMinimizer          = ifile.readline().partition('#')[0].strip().upper()
Initial               = ifile.readline().partition('#')[0].strip().upper()
MinimizeEndpoint      = ifile.readline().partition('#')[0].strip().upper()
SimPar.Nactive        = int(ifile.readline().partition('#')[0])
SimPar.ExtraForceType = int(ifile.readline().partition('#')[0])
SerialInit = bool(int(ifile.readline().partition('#')[0].strip()))
ifile.close()



#######################################
# Read in Ab Initio Software parameters
#######################################
if   AbInit.Software == 'MOLPRO':
    AbInit.MolPro_Header, AbInit.MolPro_OtherOptions, AbInit.MolPro_Basis, AbInit.MolPro_Method = readMolPro(CaseName+'.input')
elif AbInit.Software == 'GAMESS':
    AbInit.GAMESS_Header = readGAMESS(CaseName+'.input')
  
######################################
# SECRET PARAMETERS, and Overrides
numpy.random.seed(42)
if NEBMinimizer == 'QM' or NEBMinimizer == 'FIRE':
    gfac = 1.0
  
######################################
print '--------------------------------------------------'
print ' STARTING NEB WITH THE FOLLOWING INPUT PARAMETERS'
print '    Case Name         = ',CaseName
print '    Number of images  = ',Nimage
print '    Spring constant   = ',kspring
print '    gfac              = ',gfac
print '    drcrit            = ',drcrit
print '    dEcrit            = ',dEcrit
print '    Nitermax          = ',Nitermax
print '    Climbing Image    = ',ClimbingImage
print '    NEB Minimizer     = ',NEBMinimizer
print '    Initial guess     = ',Initial
print '    Minimize Endpoint = ',MinimizeEndpoint
print '    Active atoms      = ',SimPar.Nactive
print '    Extra Forces      = ',SimPar.ExtraForceType
print '--------------------------------------------------'
print ' AB INITIO PARAMETERS'
print '    Software = ',AbInit.Software
if AbInit.Software == 'MOLPRO':
    print '    Header   = ',AbInit.MolPro_Header
    print '    Others   = ',AbInit.MolPro_OtherOptions
    print '    Basis    = ',AbInit.MolPro_Basis
    print '    Method   = \n',AbInit.MolPro_Method
elif AbInit.Software == 'GAMESS':
    print '    HEADER   = ',AbInit.GAMESS_Header
  
print '--------------------------------------------------'
print ' OTHER PARAMETERS'
print '    Serial Initialize = ',SerialInit
print '--------------------------------------------------'
print ' SECRET PARAMETERS'
print '    Random number seed = ',42
if NEBMinimizer == 'QM' or NEBMinimizer == 'FIRE':
    print '    Resetting gfac = ',1
print '--------------------------------------------------'

######################################
# Preliminary checks on NEB input data
######################################
# Checks that will make the code stop
if AbInit.Software == 'GAMESS':
    print 'Hello Polly -- GAMESS Code under development'
    sys.exit()
  
if AbInit.Software != 'MOLPRO' and AbInit.Software != 'GAMESS':
    print '------> Invalid Software choice. <------'
    print '  Valid choices are :'
    print '   MolPro'
    print '   GAMESS'
    raise
if SimPar.ExtraForceType > 5 or SimPar.ExtraForceType<0 :
    print '------> Invalid ExtraForceType. <------'
    print '  Valid choices are :'
    print '  0 = No extra force'
    print '  1 = Constant'
    print '  2 = Hydrostatic'
    print '  3 = X-only'
    print '  4 = Y-only'
    print '  5 = Z-only'
    raise
if NEBMinimizer != 'SD' and NEBMinimizer != 'CG' and NEBMinimizer != 'QM' and NEBMinimizer != 'FIRE' and NEBMinimizer != 'BFGS' and NEBMinimizer != 'GBFGS':
    print '------> Invalid Minimizer. <------'
    print '  Valid choices are :'
    print '  SD    = Steepest Descent'
    print '  CG    = Conjugate Gradient'
    print '  QM    = QuickMin'
    print '  FIRE  = Fast Intertial Relaxation Engine'
    print '  BFGS  = Broyden-Fletcher-Goldfarb-Shanno'
    print '  GBFGS = Global Broyden-Fletcher-Goldfarb-Shanno'
    raise
if MinimizeEndpoint != 'NONE' and MinimizeEndpoint != 'START' and MinimizeEndpoint != 'FINAL' and MinimizeEndpoint != 'BOTH':
    print '------> Invalid choice for MinimizeEndpoint. <------'
    print 'Valid choices are none, start, final, both'
    raise

# Checks that will raise warnings
if ClimbingImage == 2 and Nimage < 5:
    print 'WARNING WARNING WARNING'
    print 'Using fewer than 5 images with two climbing images'


######################################
# Create Work Directories
######################################
WorkDir = [None]*Nimage
for img in range(Nimage):
    WorkDir[img] = CaseName+'.image.'+str(img)
    if os.path.exists(WorkDir[img]):
        print '------> Working directories from a previous simulation exist <------'
        raise
    os.mkdir(WorkDir[img])


#######################################
# Find Natom, and set DOFMask
#######################################
# One of these files has to be present
try:
    ifile = open(CaseName+'.start','r')
except:
    ifile = open(CaseName+'.chain.initial','r')
    
SimPar.Natom   = int(ifile.readline())
ifile.close()


SimPar.DOFMask = (numpy.zeros((SimPar.Natom,3)) + 1) < 0 # Creates an array of all False
if SimPar.Nactive == -1 or SimPar.Nactive == -2:
    SimPar.DOFMask = True+SimPar.DOFMask
elif SimPar.Nactive > 0:
    for iatom in range(SimPar.Nactive):
        for j in range(3):
            SimPar.DOFMask[iatom][j] = True
elif SimPar.Nactive == 0:
    print '------> Reading DOFmask from file not coded yet. <------'
    sys.exit()
else:
    print '------> Invalid number of Nactive <------'
    sys.exit()

######################################
# Create array for additional forces (in Ha/Bohr)
######################################
ExtraForceUtility.Initialize(CaseName)



########################################################################
# Read in configs from file
########################################################################

if Initial == 'INTERPOLATE':
    config       = [None]*Nimage
    config[0]    = readXYZ(CaseName+'.start')
    config[-1]   = readXYZ(CaseName+'.final')
elif Initial == 'FILE':
    print 'Reading chain from file'
    config = readchain(CaseName+'.chain.initial',Nimage)
else:
    print '------> Unknown chain initialization method <------'
    raise

########################################################################
# Minimize start and/or end, if requested. Otherwise, just get their energies
########################################################################
if AbInit.MolPro_Method == 'hf':
    NewMethod = 'hf'
else:
    NewMethod = 'hf\n'+AbInit.MolPro_Method
### Native minimizer
if MinimizeEndpoint == 'BOTH':
    print 'Minimizing both endpoints with no force, using native minimizer'
    NativeMinimize([config[0],config[-1]], [WorkDir[0],WorkDir[-1]])
    writeXYZ      (config[0],CaseName+'.start.minimized.unforced')
    writeXYZ      (config[-1],CaseName+'.final.minimized.unforced')

elif MinimizeEndpoint == 'NONE':
    print 'Getting the energies of both the endpoints'
    CreateInput     ([config[0],config[-1]],[WorkDir[0],WorkDir[-1]],NewMethod)
    ExecAbInit      ([WorkDir[0],WorkDir[-1]])
    ReadOutputEnergy([config[0],config[-1]],[WorkDir[0],WorkDir[-1]])
  
elif MinimizeEndpoint == 'START':
    print 'Minimizing start with no force, using native minimizer'
    NativeMinimize([config[0]], [WorkDir[0]])
    writeXYZ      (config[0],CaseName+'.start.minimized.unforced')

    print 'Not minimizing final, but only getting the energies'
    CreateInput     ([config[-1]],[WorkDir[-1]],NewMethod)
    ExecAbInit      ([WorkDir[-1]])
    ReadOutputEnergy([config[-1]],[WorkDir[-1]])

elif MinimizeEndpoint == 'FINAL':
    print 'Minimizing final with no force, using native minimizer'
    NativeMinimize([config[-1]], [WorkDir[-1]])
    writeXYZ      (config[-1],CaseName+'.final.minimized.unforced')

    print 'Not minimizing start, but only getting the energies'
    CreateInput     ([config[0]],[WorkDir[0]],NewMethod)
    ExecAbInit      ([WorkDir[0]])
    ReadOutputEnergy([config[0]],[WorkDir[0]])
  
else:
    print '--------> Unknown request for minimizing endpoint <--------'
    raise


### Manual Minimizer  
if SimPar.ExtraForceType > 0:
    if MinimizeEndpoint == 'BOTH':
        print 'Minimizing end points with external forces, using Manual minimizer'
        ManualMinimize([config[0],config[-1]],[WorkDir[0],WorkDir[-1]],NEBMinimizer, CaseName, drcrit, dEcrit, Fcrit)
        writeXYZ(config[0],CaseName+'.start.minimized.forced')
        writeXYZ(config[-1],CaseName+'.final.minimized.forced')
    elif MinimizeEndpoint == 'START':
        print 'Minimizing start point with external forces, using Manual minimizer'
        ManualMinimize([config[0]],[WorkDir[0]],NEBMinimizer, CaseName, drcrit, dEcrit, Fcrit)
        writeXYZ(config[0],CaseName+'.start.minimized.forced')
    elif MinimizeEndpoint == 'FINAL':
        print 'Minimizing final point with external forces, using Manual minimizer'
        ManualMinimize([config[-1]],[WorkDir[-1]],NEBMinimizer, CaseName, drcrit, dEcrit, Fcrit)
        writeXYZ(config[-1],CaseName+'.start.minimized.forced')

    # After manual minimization (if any), shift energy using config[0] as the reference
    config[-1].E = config[-1].E + ExtraForceUtility.Vext(config[-1],config[0])

    print '********************************************************************'
    print '                       WARNING WARNING WARNING '
    print '      Make sure the forced config did not spontaneously transition'
    print '********************************************************************'
  

######################################
# Create Chain, if it wasn't read from the file
######################################
if Initial == 'INTERPOLATE':
  dr=config[-1].Coord - config[0].Coord
  for iimg in range(1,Nimage-1):
    config[iimg]                       = copy.deepcopy(config[iimg-1])
    config[iimg].Coord                 = config[0].Coord + (float(iimg)/(Nimage-1))*dr
    dispmag                            = 0.0
    randisp                            = dispmag*numpy.random.rand(SimPar.Natom,3) - (0.5*dispmag)
    config[iimg].Coord[SimPar.DOFMask] = config[iimg].Coord[SimPar.DOFMask] + randisp[SimPar.DOFMask]

  
######################################
# Start NEBbing
######################################
Niter     = 0
drstep    = [None]*Nimage
drstephat = [None]*Nimage
Fnet      = [None]*Nimage

# Initialization for Quickmin
if NEBMinimizer == 'QM':
    dt = readQM(CaseName+'.input')
    Velocity  = [None]*Nimage
    for iconf in range(Nimage):
        Velocity[iconf] = 0*config[0].Coord

# Initialization for Fire
if NEBMinimizer == 'FIRE':
    alpha_start, dt_start, dtmax, fdec_dt, finc_dt, falpha, Nadjustmin = readFIRE(CaseName+'.input')
    itercut     = 0
    Velocity    = [None]       *Nimage
    Force       = [None]       *Nimage
    Mass        = [None]       *Nimage
    dt          = [dt_start]   *Nimage
    alpha       = [alpha_start]*Nimage
    for iconf in range(Nimage):
        Velocity[iconf] = 0*config[0].Coord
        Mass[iconf]     = CreateMassArray(config[iconf].ChemSym)
  
# Initialization for BFGS
if NEBMinimizer == 'BFGS':
    IHess = [numpy.identity(numpy.prod(config[0].Coord.shape),dtype='float')] * Nimage

# Initialization for Global BFGS
if NEBMinimizer == 'GBFGS':
    IHess_Global = 0.1*numpy.identity((Nimage-2)*numpy.prod(config[0].Coord.shape),dtype='float')



### Start NEBbing.
while True:

    if (Niter == 0) and (AbInit.MolPro_Method != 'hf') and (not SerialInit):
        NewMethod = 'hf\n'+AbInit.MolPro_Method
    else:
        NewMethod = AbInit.MolPro_Method
    NewMethod = NewMethod+'\nforce;varsav;table,gradx,grady,gradz;format,\'d45.35,d45.35,d45.35\''


    # Copy previous shit
    LastConfig    = copy.deepcopy(config) # (This includes the gradients and energies)
    Lastdrstep    = copy.deepcopy(drstep)
    Lastdrstephat = copy.deepcopy(drstephat)
    LastFnet      = copy.deepcopy(Fnet)



    #####  
    #####  This bit gets the forces and energies.
    #####  It could have been replaced by a single 
    #####  'getforces' routine, but the SerialInit
    #####  business forces me to split it up.
    #####  

    #Create Input files for everyone
    for iimg in range(1,Nimage-1):
        CreateInput([config[iimg]],[WorkDir[iimg]],NewMethod)

    # Run AbInit
    # If Serial Init, do some special shit for first iteration
    if SerialInit and Niter == 0 :
        print 'Doing Serial Init'

        # Create Input file for casename.final
        CreateInput([config[-1]],[WorkDir[-1]],NewMethod)

        #Calculate forces and energies of all images, including casename.final
        for iimg in range(1,Nimage):
            print 'Serial Init on image number ',iimg
            # Copy wavefunction, etc. from previous directory, and run AbInit
            for f in ['integ','wfunc']:
              shutil.copyfile(WorkDir[iimg-1]+'/molpro.'+f ,WorkDir[iimg]+'/molpro.'+f)
            ExecAbInit([WorkDir[iimg]])

        # If a minimize of <casename>.final was requested, do it.
        if MinimizeEndpoint == 'FINAL' or MinimizeEndpoint == 'BOTH':
            print 'Serial Init, re-minimizing final point'

            print 'Reminimizing with native minimizer'
            NativeMinimize([config[-1]], [WorkDir[-1]])
            writeXYZ      (config[-1],CaseName+'.final.minimized.unforced')

            if SimPar.ExtraForceType > 0:
                print 'Reminimizing with manual minimizer'
                ManualMinimize([config[-1]],[WorkDir[-1]],NEBMinimizer, CaseName, drcrit, dEcrit, Fcrit)
                writeXYZ(config[-1],CaseName+'.start.minimized.forced')

    else:
        ExecAbInit(WorkDir[1:-1])

    # Read energies, and gradients
    ReadOutputEnergy  (config[1:-1],WorkDir[1:-1])
    ReadOutputGradient(config[1:-1],WorkDir[1:-1])






    ###################################
    # Add forces, if needed
    ###################################
    if SimPar.ExtraForceType==1:
        print 'Adding constant forces'
        for iimg in range(1,Nimage-1):
          config[iimg].Gradient = config[iimg].Gradient - SimPar.ExtraForce
          config[iimg].E        = config[iimg].E + ExtraForceUtility.Vext(config[iimg],config[0])

    elif SimPar.ExtraForceType >= 2:
        print 'Adding Hydrostatic Forces'
        for iimg in range(1,Nimage-1):
          config[iimg].Gradient = config[iimg].Gradient - ExtraForceUtility.FHydro(SimPar.ExtraForcePressure,config[iimg].Coord) 
          config[iimg].E        = config[iimg].E + ExtraForceUtility.Vext(config[iimg],config[0])


    ### Find isad, drmax, dE, Fgradmax
    isad        = 0
    Esad        = config[0].E
    drmax       = [0]*Nimage
    dE          = [0]*Nimage
    Fgradmax    = [0]*Nimage
    Fspringmax  = [0]*Nimage
    Fnetmax     = [0]*Nimage
    InterMin    = [False]*Nimage
    for iimg in range(1,Nimage-1):
        Fgradmax[iimg]  = numpy.amax(abs(config[iimg].Gradient[SimPar.DOFMask] ) )

        if (config[iimg].E < config[iimg+1].E) and (config[iimg].E < config[iimg-1].E):
            InterMin[iimg] = True
        if config[iimg].E > Esad:
            Esad = config[iimg].E
            isad = iimg
        if Niter != 0:
            drmax[iimg]       = numpy.amax(abs(drstep[iimg][SimPar.DOFMask]))
            Fspringmax[iimg]  = numpy.amax(abs(Fspring[iimg][SimPar.DOFMask] ) )
            Fnetmax[iimg]     = numpy.amax(abs(Fnet[iimg][SimPar.DOFMask] ) )
            dE[iimg]          = (config[iimg].E - LastConfig[iimg].E)


    # Check for termination
    Terminate = False
    if Niter != 0:
        if (dE[isad] < dEcrit) and (max(drmax) < drcrit) and (Fgradmax[isad] < Fcrit):
            Terminate = True
        if Niter > Nitermax:
            Terminate = True
    if Nitermax <= 0:
        Terminate = True

    ### Print shit out, and write current chain to file
    if (Niter == 0) or (Niter%1==0) or Terminate:
        print '================================================================================================================'
        print 'Iteration number: ',Niter
        print '     Img   E (Ha) Erel(eV) dE/dit(Ha)  Fgradmax  Fsprngmax Fspr/Fgrad    Fnetmax  drmax(Ang)'
        for iimg in range(Nimage):
          if iimg == isad:
            HeadString='     '
            TailString='<---^'
          elif InterMin[iimg] == True:
            HeadString='     '
            TailString='<---V'
          else:
            HeadString='     '
            TailString='     '

          if Fgradmax[iimg] < 1e-25:
            ForceRatio = 0.0
          else:
            ForceRatio = Fspringmax[iimg]/Fgradmax[iimg]

          print HeadString,
          print '{0:2d} {1:8.3f} {2:7.3f} {3:10.2e} {4:10.2e} {5:10.2e} {6:10.2e} {7:10.2e} {8:10.2e} '.format(iimg,config[iimg].E,(config[iimg].E-config[0].E)*27.21,dE[iimg],Fgradmax[iimg],Fspringmax[iimg],ForceRatio,Fnetmax[iimg],drmax[iimg] ),
          print TailString

        print '================================================================================================================'

        if Niter == 0:
            writechain(config,CaseName+'.chain.0')
            writeXYZ(config[isad],CaseName+'.sad.0')
        writechain(config,CaseName+'.chain')
        writeXYZ(config[isad],CaseName+'.sad')

    if Terminate:
        break




    ############################################################
    ## Compute unit tangent, net force, for all images
    ############################################################
    drstep = [None]*Nimage
    Fspring = [None]*Nimage
    for iimg in range(1,Nimage-1):
        Vleft     = config[iimg  ].Coord - config[iimg-1].Coord
        Vright    = config[iimg+1].Coord - config[iimg  ].Coord
        modVleft  = numpy.sqrt( numpy.sum( numpy.multiply ( Vleft ,Vleft  ) ) )
        modVright = numpy.sqrt( numpy.sum( numpy.multiply ( Vright,Vright ) ) )


        ## Compute Tangent (Using improved tangent estimate)
        if  ( (config[iimg].E > config[iimg-1].E) and (config[iimg].E > config[iimg+1].E) ) or \
            ( (config[iimg].E < config[iimg-1].E) and (config[iimg].E < config[iimg+1].E) ):
            Emax = max(   abs(config[iimg].E-config[iimg-1].E),  abs(config[iimg].E-config[iimg+1].E)   )
            Emin = min(   abs(config[iimg].E-config[iimg-1].E),  abs(config[iimg].E-config[iimg+1].E)   )
            if config[iimg+1].E > config[iimg-1].E:
                T = Emax*Vright + Emin*Vleft
            else:
              T = Emin*Vright + Emax*Vleft
        elif (config[iimg].E > config[iimg-1].E):
            T = Vright
        else:
            T = Vleft
        That       = T / numpy.sqrt(numpy.sum(numpy.multiply(T,T)))


        Fspring[iimg] =  kspring*(modVright - modVleft)*That
        Fgrad         = -gfac*(copy.deepcopy(config[iimg].Gradient))
        Fparallel     =  numpy.sum(numpy.multiply(Fgrad,That)) * That
        Fperp         =  Fgrad - Fparallel
        Fnet[iimg]    =  Fspring[iimg] + Fperp

        # Climbing image
        if (ClimbingImage==1 and iimg == isad) or \
           (ClimbingImage==2 and (iimg==isad-1 or iimg==isad+1) ):
             Fnet[iimg] = Fgrad - 2*numpy.sum(numpy.multiply(Fgrad,That))*That






    ############################################################
    ## Based on minimizer, compute dr for all images.
    ############################################################
    #---------------------------------------------
    if NEBMinimizer == 'SD':
        for iimg in range(1,Nimage-1):
            drstep[iimg]  =  copy.deepcopy(Fnet[iimg])
        #---------------------------------------------
    elif NEBMinimizer == 'CG':
        for iimg in range(1,Nimage-1):
            if Niter == 0:
                drstep[iimg] = copy.deepcopy(Fnet[iimg])
            else:
                gamma = numpy.sum(numpy.multiply(Fnet[iimg],Fnet[iimg])) / numpy.sum(numpy.multiply(LastFnet[iimg],LastFnet[iimg]))
                drstep[iimg] = Fnet[iimg] + gamma*LastFnet[iimg]
      #---------------------------------------------
    elif NEBMinimizer == 'QM':
        for iimg in range(1,Nimage-1):
            Fhat     = Fnet[iimg] / numpy.sqrt(numpy.sum(numpy.multiply(Fnet[iimg],Fnet[iimg])))
            VdotFhat = numpy.sum(numpy.multiply(Velocity[iimg],Fhat))
            if VdotFhat <= 0:
                print '*** Quickmin zeroing veocity ***'
                Velocity[iimg] = 0*Velocity[iimg]
            else:
                Velocity[iimg] = VdotFhat * Fhat

            drstep[iimg] = dt * Velocity[iimg]
            print 'QM: iimg, vel_max_old, vel_max_new = ',iimg,numpy.amax(abs(Velocity[iimg])),
            Velocity[iimg] = Velocity[iimg]+dt*Fnet[iimg]
            print numpy.amax(abs(Velocity[iimg]))
      #---------------------------------------------
    elif NEBMinimizer == 'FIRE':

        # Compute forces, velocities, and decide if we need to slow down
        Steadied = 0
        for iimg in range(1,Nimage-1):
            Force[iimg] = copy.deepcopy(Fnet[iimg])
            ModF        = numpy.sqrt(numpy.sum(numpy.multiply(Force[iimg],Force[iimg])))
            ModV        = numpy.sqrt(numpy.sum(numpy.multiply(Velocity[iimg],Velocity[iimg])))
            Fhat        = Force[iimg]/ModF
            P           = numpy.sum(numpy.multiply(Force[iimg],Velocity[iimg]))
            Velocity[iimg] = (1-alpha[iimg])*Velocity[iimg] + alpha[iimg]*ModV*Fhat
            if P <=0 and Niter > 0: #Steady on
                print 'Need to steady -- ',iimg
                Steadied = 1 # Change happens later

        #Speed up, if it is appropriate to do so
        for iimg in range(1,Nimage-1):
            if Steadied == 0 and (Niter-itercut)>Nadjustmin:
                #print 'Speed up: iconf, Niter, dt_old, dt_new = ',iimg,Niter,dt[iimg],
                dt[iimg]    = min(dt[iimg]*finc_dt,dtmax)
                #print dt[iimg]
                alpha[iimg] = alpha[iimg] * falpha

        # Otherwise, slow everyone down
        if Steadied == 1:
            for iimg in range(1,Nimage-1):
                #print 'Steady: iconf, Niter, dt_old, dt_new = ',iimg,Niter,dt[iimg],
                Velocity[iimg] = 0*Velocity[iimg]
                dt[iimg]       = dt[iimg]*fdec_dt
                #print dt[iimg]
                itercut           = Niter
                alpha[iimg]    = alpha_start



        #Euler Integrate positions and velocities
        for iimg in range(1,Nimage-1):
            drstep[iimg] = dt[iimg]*Velocity[iimg]
            Velocity[iimg] = Velocity[iimg] + dt[iimg]*(Force[iimg]/Mass[iimg])

        #---------------------------------------------
    elif NEBMinimizer == 'BFGS':
        print 'BFGS Step'
        print 'WARNING WARNING WARNING WARNING'
        print 'WARNING WARNING WARNING WARNING'
        print 'Might need to revisit BFGS minimizer when some DOFs are not free'

        for iimg in range(1,Nimage-1):
            if Niter > 0:
                s = Lastdrstep[iimg]
                y = Fnet[iimg] - LastFnet[iimg]
                IHess[iimg] = updateH(IHess[iimg],s,y)
            drstep[iimg] = - multiplyHC(IHess[iimg],Fnet[iimg])
        #---------------------------------------------
    elif NEBMinimizer == 'GBFGS':
         print 'Global BFGS Step'
         print 'WARNING WARNING WARNING WARNING'
         print 'WARNING WARNING WARNING WARNING'
         print 'Might need to revisit G-BFGS minimizer when some DOFs are not free'

         if Niter > 0:
             s = Lastdrstep

             y = [None]*(Nimage)
             for iimg in range(1,Nimage-1):
                 y[iimg] = Fnet[iimg] - LastFnet[iimg]

             IHess_Global = updateHG(IHess_Global,s[1:Nimage-1],y[1:Nimage-1])


         drstep[1:Nimage-1] = multiplyHC_global( -IHess_Global,Fnet[1:Nimage-1]) # Note the negative sign inside the brackets


         #---------------------------------------------
    else:
        print 'Unknown NEBMinimizer --',NEBMinimizer
        sys.exit()









    ############################################################
    ## Move all images, but limit the dr
    ############################################################
    for iimg in range(1,Nimage-1):
        #Project out Rigid body DOFs, if needed
        if SimPar.Nactive == -2:
            SubtractThis = numpy.zeros((SimPar.Natom,3))

            #Translation along X, Y and Z
            for iaxis in range(3):
              transvec          = numpy.zeros((SimPar.Natom,3))
              transvec[:,iaxis] = 1
              transvec          = vec.vunit(transvec)
              SubtractThis     += numpy.vdot(drstep[iimg],transvec) * transvec

            # Rotational degrees of freedom
            # X
            transvec      = transform.rotatex(config[iimg].Coord)
            transvec      = vec.vunit(transvec)
            SubtractThis += numpy.vdot(drstep[iimg],transvec) * transvec

            # Y
            transvec      = transform.rotatey(config[iimg].Coord)
            transvec      = vec.vunit(transvec)
            SubtractThis += numpy.vdot(drstep[iimg],transvec) * transvec

            # Z
            transvec      = transform.rotatez(config[iimg].Coord)
            transvec      = vec.vunit(transvec)
            SubtractThis += numpy.vdot(drstep[iimg],transvec) * transvec

            drstep[iimg] -= SubtractThis

        # Limit the dr (This is actually a bad idea, but doesn't matter because our drcrit is generally large)
        mask                               = abs(drstep[iimg])>drcrit
        drstep[iimg][mask]                 = drcrit*numpy.sign(drstep[iimg][mask])
        config[iimg].Coord[SimPar.DOFMask] = config[iimg].Coord[SimPar.DOFMask] + drstep[iimg][SimPar.DOFMask]


    ### Update iterations counter
    Niter += 1

### END OF LOOP ###



print '******************************************************'
print '                      END OF NEB'
print '******************************************************'
print ' Status: ',
if Niter > Nitermax:
    print 'BALED BEFORE CONVERGENCE'
    print '          THE FOLLOWING INFORMATION IS PROBABLY CLOSE TO CORRECT'
    print '          BUT THERE MAY BE SOME ERRORS IN THE SOLUTION'
    print '          CHECK PARAMETERS, AND SEE WHAT HAPPENED'
else:
    print 'CONVERGED'
  
print 'All energies in Ha'
print 'Saddle point image isad = ',isad
print 'E_start      = ',config[0].E
print 'E_final      = ',config[isad].E
print 'E_final      = ',config[-1].E
print 'E_a forward  = ',config[isad].E-config[0].E
print 'E_a backward = ',config[isad].E-config[-1].E

writechain(config,CaseName+'.chain.converged')
writeXYZ(config[isad],CaseName+'.saddle.converged')

