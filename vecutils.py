import numpy

def vmag (vin):
    return numpy.sqrt(numpy.vdot(vin,vin))

def vunit(vin):
    return vin / vmag(vin)

def vecproj(v1,v2):
    v2mag = vmag(v2)
    return (v2 / v2mag) * numpy.vdot(v1,v2/v2mag)
