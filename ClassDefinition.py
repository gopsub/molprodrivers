import numpy
import copy


#==================================================================================
class ConfigClass:
    
#   Natom    : Scalar,      Number of atoms
#   E        : Scalar,      Energy of configuration. Initialized to zero
#   Gradient : Numpy array, Gradient, initialized to zero
#   ChemSym  : List,        Chemical symbol of each atom
#   Coord    : Numpy array, XYZ coordinates of each atom. First index is atom number,
#                           Second index is coordinate direction
#==================================================================================
    def __init__(self):
        self.ChemSym  = []
        self.Coord    = numpy.array([]).reshape(0,3)
        self.Natom    = 0
        self.E        = 0.0
        self.Gradient = 0*self.Coord



